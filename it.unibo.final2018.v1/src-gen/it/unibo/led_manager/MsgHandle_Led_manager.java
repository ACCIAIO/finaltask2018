/* Generated by AN DISI Unibo */ 
package it.unibo.led_manager;
import it.unibo.qactors.QActorContext;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.akka.QActorMsgQueue;

public class MsgHandle_Led_manager extends QActorMsgQueue{
	public MsgHandle_Led_manager(String actorId, QActorContext myCtx, IOutputEnvView outEnvView )  {
		super(actorId, myCtx, outEnvView);
	}
}
