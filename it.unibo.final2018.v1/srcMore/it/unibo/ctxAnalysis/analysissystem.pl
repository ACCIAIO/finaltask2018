%====================================================================================
% Context ctxAnalysis  SYSTEM-configuration: file it.unibo.ctxAnalysis.analysisSystem.pl 
%====================================================================================
context(ctxanalysis, "localhost",  "TCP", "8039" ).  		 
%%% -------------------------------------------
qactor( controller , ctxanalysis, "it.unibo.controller.MsgHandle_Controller"   ). %%store msgs 
qactor( controller_ctrl , ctxanalysis, "it.unibo.controller.Controller"   ). %%control-driven 
qactor( thermometer , ctxanalysis, "it.unibo.thermometer.MsgHandle_Thermometer"   ). %%store msgs 
qactor( thermometer_ctrl , ctxanalysis, "it.unibo.thermometer.Thermometer"   ). %%control-driven 
qactor( clock , ctxanalysis, "it.unibo.clock.MsgHandle_Clock"   ). %%store msgs 
qactor( clock_ctrl , ctxanalysis, "it.unibo.clock.Clock"   ). %%control-driven 
qactor( start_test , ctxanalysis, "it.unibo.start_test.MsgHandle_Start_test"   ). %%store msgs 
qactor( start_test_ctrl , ctxanalysis, "it.unibo.start_test.Start_test"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

