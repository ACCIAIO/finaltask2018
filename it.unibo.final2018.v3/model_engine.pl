% Remember: model entries are in another file (castle)

eval(ge, X, X) :- !. 
eval(ge, X, V) :- eval(gt, X , V).

eval(le, X, X) :- !.
eval(le, X, V) :- eval(lt, X, V).

% changeModelAction must be defined by the actor
changeModel(TYPE, VALUE) :- model(TYPE, OLD), replaceRule(model(TYPE, OLD), model(TYPE, VALUE)), !, changeModelAction(TYPE, VALUE).
changeModel(TYPE, VALUE) :- addRule(model(TYPE, VALUE)), !, changeModelAction(TYPE, VALUE).

emitevent(EVID, EVCONTENT) :- actorobj(Actor), Actor <- emit(EVID, EVCONTENT).