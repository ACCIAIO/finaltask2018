function log(message) {
	document.getElementById("cmdconsole").innerHTML += "QActorBridge.js:\t" + message + '<br/>';
}
	
var connected = false;
var sock;

addLoadEvent(function() {
	/* SET UP WEBSOCKET */
	var host = "localhost:8080";
	//var host = document.location.host
	log("server IP= "+ host + " connected=" + connected);
    //var sock = new WebSocket("ws://"+document.location.host, "protocolOne");      
    sock = new WebSocket("ws://" + host, "protocolOne");
	sock.onopen = function (event) {
		log("Connection established");
		connected = true;
	};
	sock.onmessage = function (event) {
		log('sent message: ' +event.data);
	}
	sock.onerror = function (event) {
		log('WebSocket Error: ' +  event);
	};
});

function send(message) {
		if( connected ){
			log("Sending message: " + message);
			sock.send(message);
			log("Message sent");
		}
		else {
			log("Can't send message: " +  message);
		}
}; 