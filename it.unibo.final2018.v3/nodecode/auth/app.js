var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');

/* <Mongoose>*/
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/natali');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function(){
	//We're connected!
	console.log('Connected to MongoDB through Mongoose.');
});


/* </Mongoose> */

var bodyparser = require('body-parser');

/* REQUIRE ROUTERS */
var indexRouter = 	require('./routes/index');
var loginRouter = 	require('./routes/login');
var logoutRouter = 	require('./routes/logout');
var commandRouter = require('./routes/command');
var signupRouter = require('./routes/signup');
var usersRouter = require('./routes/users');
var wipRouter 	= require('./routes/wip');

var app = express();

// view engine setup
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(logger('dev'));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
	secret: 'iamabanana',
	resave: true,
	saveUninitialized: false
}));

/* ROUTERS */
app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/logout', logoutRouter);
app.use('/users', usersRouter);
app.use('/command', commandRouter);
app.use('/signup', signupRouter);
app.use('/about', wipRouter);	// WIP


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', {title: 'ERROR', error: err.message});
});

app.listen(3000, function(req, res){
	console.log('RAPIDASH authentication server online.');
});

module.exports = app;
