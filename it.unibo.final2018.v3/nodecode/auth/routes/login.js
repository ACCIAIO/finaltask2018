var express = require('express');
var router = express.Router();
var User = require('../models/user');

/* POST login*/
router.post('/reqLogin', function(req, res, next){
	
	console.log('Richiesta di login: '+req.body.username+", pwd: "+req.body.pwd);
	if(req.body.username && req.body.pwd){
		User.authenticate(req.body.username, req.body.pwd, function(err, user){
			if(err){
				next(err);
			}else{
				req.session.user = user;
				res.redirect('/command');
			}
		});
	}
});


/* Login */
router.get('/', function(req, res){
	if(req.session.user){
		//Already logged in
		res.render('command', {title: 'Command', user: req.session.user});
	}else{		
		res.render('login', { title: 'Login' });
	}
});

/* Logout*/
router.get('/logout', function(req, res){
	if(req.session.user){
		console.log(req.session.user.username + 'logged out.');
		delete req.session.user;
	}
	return res.redirect("/");
});

module.exports = router;
