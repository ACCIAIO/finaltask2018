var express = require('express');
var router = express.Router();

var User = require('../models/user');

/* POST signup*/
router.post('/reqSignUp', function(req, res, next){
	console.log('Richiesta di sign-up: '+req.body.username+", pwd: "+req.body.pwd);
	if(req.body.username &&
			req.body.pwd &&
			req.body.pwdConf &&
			req.body.name &&
			req.body.last_name){
		
		var userData = {
				username: req.body.username,
				password: req.body.pwd,
				name: req.body.name,
				last_name: req.body.last_name
		}
		
		
		if(req.body.email){
			userData.email = req.body.email;
		}
		
		if(req.body.role){
			userData.role = req.body.role;
		}
		
		if(req.body.bio){
			userData.bio = req.body.bio;
		}
		
		//Use schema.create to insert data into the db
		User.create(userData, function(err, user){
			if(err){
				return next(err);
			}else{
				return res.redirect('/signup/success');
			}
		});
	}
});

/* Sign-up page */
router.get('/', function(req, res){
	console.log('Get di signup raggiunta');
	var user;
	if(req.session.user){
		user = req.session.user;
	}
	res.render('signup', {title: 'Sign-up', user: user});
});

/* GET post sign-up page */
router.get('/success', function(req, res){
	console.log('Iscrizione avvenuta con successo');
	var user;
	if(req.session.user){
		user = req.session.user;
	}
	res.render('signup_success', {title: 'Iscrizione completa', user: user});
});

module.exports = router;
