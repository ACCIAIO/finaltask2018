var express = require('express');
var router = express.Router();

/* Command */
router.get('/', function(req, res){
	if(req.session.user){		
		res.render('command', {title: 'Command', user: req.session.user});
	}else{
		res.redirect('/login');
	}
});

module.exports = router;
