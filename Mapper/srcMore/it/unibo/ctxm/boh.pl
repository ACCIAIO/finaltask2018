%====================================================================================
% Context ctxm  SYSTEM-configuration: file it.unibo.ctxm.boh.pl 
%====================================================================================
context(ctxm, "localhost",  "TCP", "7000" ).  		 
%%% -------------------------------------------
qactor( mapper , ctxm, "it.unibo.mapper.MsgHandle_Mapper"   ). %%store msgs 
qactor( mapper_ctrl , ctxm, "it.unibo.mapper.Mapper"   ). %%control-driven 
qactor( test , ctxm, "it.unibo.test.MsgHandle_Test"   ). %%store msgs 
qactor( test_ctrl , ctxm, "it.unibo.test.Test"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

