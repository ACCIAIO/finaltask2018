package it.unibo.mapper;

import java.util.Arrays;
import java.util.HashMap;

import org.apache.commons.lang3.tuple.Pair;

public class Map {
	
	public static final int UNKNOWN = 0, NAVIGATED = 1, UNREACHABLE = 2; 
	private final static java.util.Map<String, Pair<Integer, Integer>> directionsToOffset;
	
	static {
		directionsToOffset = new HashMap<>();
		directionsToOffset.put("up", Pair.of(0, -1));
		directionsToOffset.put("right", Pair.of(1, 0));
		directionsToOffset.put("down", Pair.of(0, 1));
		directionsToOffset.put("left", Pair.of(-1, 0));
	}
	
	private int[][] map;
	private final int increment;
	
	private int bX, bY;
	
	private void ensureCapacity(int x, int y) {
		int[][] tmp = map;
		if(map.length <= x || map[0].length <= y) {
			int nl = map.length + increment; 
			map = new int[nl][nl];
			for(int i = 0; i < tmp.length; i++) {
				map[i] = Arrays.copyOf(tmp[i], nl);
			}
		}
	}
	
	public int getSize() {
		return map.length;
	}
	
	public int getBoundaryX() {
		return bX;
	}
	
	public int getBoundaryY() {
		return bY;
	}
	
	public int getSlot(int x, int y) {
		if(0 > x || 0 > y)
			throw new IllegalArgumentException("coordinates must be positive");
		if(map.length <= x || map[0].length <= y)
			return UNKNOWN;
		return map[x][y];
	}
	
	public int getSlotRelativeTo(int x, int y, String direction) {
		Pair<Integer, Integer> p = directionsToOffset.get(direction);
		return getSlot(x + p.getLeft(), y + p.getRight());
	}
	
	public void discoverSlot(int x, int y, int value) {
		if(0 > x || 0 > y)
			throw new IllegalArgumentException("coordinates must be positive");
		if(UNKNOWN > value || UNREACHABLE < value)
			throw new IllegalArgumentException("only acceptable values are UNKNOWN, NAVIGATED or UNREACHABLE");
		ensureCapacity(x, y);
		map[x][y] = value;
		
		if(UNREACHABLE == value) {
			bX = (bX > x) ? bX : x;
			bY = (bY > y) ? bY : y;
		}
	}
	
	public Map() {
		this(32);
	}
	
	public Map(int initialDimension) {
		if(0 >= initialDimension)
			throw new IllegalArgumentException("the initial dimension must be strictly positive");
		increment = initialDimension;
		map = new int[increment][increment];
		bX = bY = -1;
	}

}
