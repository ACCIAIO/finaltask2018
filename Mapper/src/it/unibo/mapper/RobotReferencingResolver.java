package it.unibo.mapper;

import java.util.HashMap;

public class RobotReferencingResolver {

	private final static java.util.Map<String, String> directionsToAction;
	static {
		directionsToAction = new HashMap<>();
		directionsToAction.put("upright", "a");
		directionsToAction.put("rightdown", "a");
		directionsToAction.put("downleft", "a");
		directionsToAction.put("leftup", "a");
		
		directionsToAction.put("updown", "a");
		directionsToAction.put("downup", "a");
		directionsToAction.put("leftright", "a");
		directionsToAction.put("rightleft", "a");
		
		directionsToAction.put("rightup", "d");
		directionsToAction.put("downright", "d");
		directionsToAction.put("leftdown", "d");
		directionsToAction.put("upleft", "d");
	}
	
	public static String resolve(String abs, String dir) {
		return directionsToAction.getOrDefault(abs + dir, "w");
	}
}
