%====================================================================================
% Context mock  SYSTEM-configuration: file it.unibo.mock.rapidash.pl 
%====================================================================================
context(mock, "localhost",  "TCP", "9003" ).  		 
%%% -------------------------------------------
qactor( mock_robot , mock, "it.unibo.mock_robot.MsgHandle_Mock_robot"   ). %%store msgs 
qactor( mock_robot_ctrl , mock, "it.unibo.mock_robot.Mock_robot"   ). %%control-driven 
qactor( led_manager , mock, "it.unibo.led_manager.MsgHandle_Led_manager"   ). %%store msgs 
qactor( led_manager_ctrl , mock, "it.unibo.led_manager.Led_manager"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

