package it.unibo.behaviour;
 
import java.time.LocalTime;
import java.util.Timer;
import java.util.TimerTask;
 
import it.unibo.qactors.akka.QActor;
 
public class clockUtils {
        
        private static boolean in;
        private static Timer timer;
        
        public static void setBounds(QActor actor, String d, String u) {
                int down = Integer.parseInt(d) % 24;
                int up = Integer.parseInt(u) % 24;
                int h = LocalTime.now().getHour();
                in = h >= down && h < up;
                actor.emit("outOfTime", "outOfTime(" + Boolean.toString(!in) + ")");
                if(null != timer)
                	timer.cancel();
                timer = new Timer();
                TimerTask tt = new TimerTask() {
 
                        @Override
                        public void run() {
                                int h = LocalTime.now().getHour();
                                if(in != (in = h >= down && h < up))
                                       actor.emit("outOfTime", "outOfTime(" + 
                                                   Boolean.toString(!in) + ")");
                        }
                        
                };
                timer.scheduleAtFixedRate(tt, 0, 60000);
        }
}
