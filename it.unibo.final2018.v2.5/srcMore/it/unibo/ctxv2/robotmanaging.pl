%====================================================================================
% Context ctxv2  SYSTEM-configuration: file it.unibo.ctxv2.robotManaging.pl 
%====================================================================================
context(ctxv2, "localhost",  "TCP", "8090" ).  		 
%%% -------------------------------------------
qactor( controller , ctxv2, "it.unibo.controller.MsgHandle_Controller"   ). %%store msgs 
qactor( controller_ctrl , ctxv2, "it.unibo.controller.Controller"   ). %%control-driven 
qactor( thermometer , ctxv2, "it.unibo.thermometer.MsgHandle_Thermometer"   ). %%store msgs 
qactor( thermometer_ctrl , ctxv2, "it.unibo.thermometer.Thermometer"   ). %%control-driven 
qactor( clock , ctxv2, "it.unibo.clock.MsgHandle_Clock"   ). %%store msgs 
qactor( clock_ctrl , ctxv2, "it.unibo.clock.Clock"   ). %%control-driven 
qactor( start_test , ctxv2, "it.unibo.start_test.MsgHandle_Start_test"   ). %%store msgs 
qactor( start_test_ctrl , ctxv2, "it.unibo.start_test.Start_test"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(invalid_handler,ctxv2,"it.unibo.ctxv2.Invalid_handler","invalidev").  
%%% -------------------------------------------

