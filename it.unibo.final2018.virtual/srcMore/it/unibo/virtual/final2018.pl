%====================================================================================
% Context virtual  SYSTEM-configuration: file it.unibo.virtual.final2018.pl 
%====================================================================================
context(virtual, "localhost",  "TCP", "9003" ).  		 
%%% -------------------------------------------
qactor( virtual_robot , virtual, "it.unibo.virtual_robot.MsgHandle_Virtual_robot"   ). %%store msgs 
qactor( virtual_robot_ctrl , virtual, "it.unibo.virtual_robot.Virtual_robot"   ). %%control-driven 
qactor( led_manager , virtual, "it.unibo.led_manager.MsgHandle_Led_manager"   ). %%store msgs 
qactor( led_manager_ctrl , virtual, "it.unibo.led_manager.Led_manager"   ). %%control-driven 
qactor( sonar , virtual, "it.unibo.sonar.MsgHandle_Sonar"   ). %%store msgs 
qactor( sonar_ctrl , virtual, "it.unibo.sonar.Sonar"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

