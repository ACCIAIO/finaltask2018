package it.unibo.controls;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import it.unibo.qactors.akka.QActor;

public class virtualControls {
	private final static Map<String, String> conversions = new HashMap<>();
	private final static Map<String, Integer> times = new HashMap<>();
	static {
		conversions.put("w", "moveForward");
		conversions.put("a", "turnLeft");
		conversions.put("s", "moveBackward");
		conversions.put("d", "turnRight");
		conversions.put("h", "alarm");

		times.put("w", 200);
		times.put("a", 800);
		times.put("s", 200);
		times.put("d", 800);
		times.put("h", 800);
	}

	private final static String hostName = "localhost";
	private final static int port = 8999;
	private final static String sep = ";";

	private static Socket clientSocket;
	private static PrintWriter outToServer;

	public static void initClientConn(QActor actor) throws Exception {
		if(null != clientSocket)
			return;
		clientSocket = new Socket(hostName, port);
		outToServer = new PrintWriter(clientSocket.getOutputStream());
	}

	public static void sendCmd(QActor actor, String msg) {
		if (outToServer == null)
			return;
		if(!conversions.keySet().contains(msg))
			return;
		String jsonString = "{ \"type\": \"" + conversions.get(msg) + "\", \"arg\":" + times.get(msg) + " }";
		System.out.println("[VIRTUAL]\t" + jsonString);
		JSONObject jsonObject = new JSONObject(jsonString);
		msg = sep + jsonObject.toString() + sep;
		outToServer.println(msg);
		outToServer.flush();
	}

	
}
