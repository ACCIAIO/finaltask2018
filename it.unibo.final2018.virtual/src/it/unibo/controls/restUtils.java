package it.unibo.controls;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import it.unibo.qactors.akka.QActor;

public class restUtils {

	private static void send(String who, boolean on) throws IOException {
		try {

			URL url = new URL(who);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("PUT");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.getOutputStream().write(("{\"on\": " + on + "}").getBytes());
			
			System.out.println(conn.getResponseCode());
			conn.disconnect();
			
		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
	}
	
	public static void sendOn(QActor actor, String who) throws IOException {
		send(who, true);
	}
	
	public static void sendOff(QActor actor, String who) throws IOException {
		send(who, false);
	}
}
