package it.unibo.controls;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import org.json.JSONObject;

import it.unibo.qactors.akka.QActor;

public class virtualSonarControls {

	private final static String hostName = "localhost";
	private final static int port = 8999;

	private static Socket clientSocket;
	private static BufferedReader inFromServer;
	private static boolean sonar2Found = false;

	public static void initClientConn(QActor actor) throws Exception {
		if(null != clientSocket)
			return;
		clientSocket = new Socket(hostName, port);
		inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
	}
	
	public static void receiveMessage(QActor actor) throws Exception {
		while(true) {
			String msg = inFromServer.readLine();
			msg = msg.replace(";", "");
			System.out.println(msg);
	
			JSONObject jsonObject = new JSONObject(msg);
			String type = jsonObject.getString("type");
			jsonObject = jsonObject.getJSONObject("arg");
	
			if (type.equals("sonar-activated")) {
				String sonarName = jsonObject.getString("sonarName");
				int distance = jsonObject.getInt("distance");
	
				if (sonarName.equals("sonar1"))
					actor.emit("sonar1", "sonar(" + distance + ")");
				else {
					if (!sonar2Found) {
						sonar2Found = true;
						try {
							actor.emit("sonar2", "sonar(" + distance + ")");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			} else if (type.equals("collision")) {
				String objectName = jsonObject.getString("objectName");
				if (objectName.contains("moving-obstacle"))
					actor.emit("movingCollision", "collision");
				else
					actor.emit("fixedCollision", "collision");
				System.out.println("Collision emitted.");
			} else if (msg.contains("webpage-ready")) {
				System.out.println("Web page ready.");
			}
		}
	}
}
