package it.unibo.controls;

import java.io.File;
import java.io.IOException;

import it.unibo.qactors.akka.QActor;

public class virtualStartup {

	private static boolean started = false;
	
	public synchronized static void start(QActor actor) {
		if(started) return;
		try {
			ProcessBuilder builder = new ProcessBuilder("cmd", "/c", "start", "startServer.bat");
			builder.directory(new File("virtual_bot"));
			builder.start();
			started = true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
