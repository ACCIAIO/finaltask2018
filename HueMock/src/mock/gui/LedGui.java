package mock.gui;

import java.awt.Color;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import it.aperture.swing.Frame;
import it.aperture.swing.PaintEventArgs;
import it.aperture.swing.Panel;

public class LedGui extends Frame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean on = false;
	private final Panel contentPane = new Panel();
	private final JLabel ledLabel = new JLabel(new ImageIcon(this.getClass().getResource("led.png")));
	
	public boolean isOn() {
		return on;
	}
	
	public void toggle() {
		on = !on;
		contentPane.repaint();
	}
	
	public void turnOff() {
		on = false;
		contentPane.repaint();
	}
	
	public LedGui(int n) {
		this.setTitle("HUE v1.0 - id " + n);
		this.setLocationRelativeTo(null);
		this.setContentPane(contentPane);
		contentPane.setBackground(Color.decode("#003182"));
		contentPane.getPainted().add(this::onPaint);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		contentPane.add(ledLabel);
		this.setDefaultCloseOperation(Frame.DISPOSE_ON_CLOSE);
		this.setResizable(false);
		this.pack();
	}
	
	private void onPaint(PaintEventArgs args) {
		Color c;
		if(!on) c = Color.darkGray;
		else c = Color.red;
		args.getGraphics().setColor(c);
		args.getGraphics().fillOval(contentPane.getWidth() / 2 - 250, contentPane.getHeight() / 2 - 250, 500, 500);
	}
	
}
