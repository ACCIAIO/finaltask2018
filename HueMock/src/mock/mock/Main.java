package mock.mock;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import mock.controller.Controller;
import mock.gui.LedGui;

public class Main {

	
	public static void main(String[] args) throws IOException {
		String user = "acciaio";
		int n = 1;
		Controller controller = new Controller();
		HttpServer server = HttpServer.create(new InetSocketAddress(8000), 20);

		HttpHandler lightsHandler = new HttpHandler() {

			@Override
			public void handle(HttpExchange ex) throws IOException {
				controller.handleLights(ex);
			}
			
		};
		
		HttpHandler singleHandler = new HttpHandler() {
			
			@Override
			public void handle(HttpExchange ex) throws IOException {
				controller.handleSingle(ex);
			}
			
		};
		
		HttpHandler toggleHandler = new HttpHandler() {
			
			@Override
			public void handle(HttpExchange ex) throws IOException {
				controller.handleSwitch(ex);
			}
			
		};
		
		if(0 != args.length)
			try {
				n = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				user = args[0];
			}
		if(1 < args.length)
			user = args[1];
		
		for(int i = 0; i < n; i++) {
			controller.addLight(new LedGui(i), i);
			server.createContext("/api/" + user + "/lights/id/" + i + "/state", toggleHandler);
			server.createContext("/api/" + user + "/lights/id/" + i, singleHandler);
		}
		server.createContext("/api/" + user + "/lights", lightsHandler);
		
		controller.start();	
		
		server.setExecutor(null);
		server.start();
	}

}
