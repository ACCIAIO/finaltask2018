package mock.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.sun.net.httpserver.HttpExchange;

import mock.gui.LedGui;

public class Controller{

	private final Map<Integer, LedGui> guis = new HashMap<>();
	
	public int countLights() {
		return guis.size();
	}
	
	public void addLight(LedGui ledGui, int id) {
		guis.put(id, ledGui);
	}
	
	public void start() {
		for(LedGui gui : guis.values())
			gui.setVisible(true);
	}

	public void handleSingle(HttpExchange ex) throws IOException {
		String[] uriSplit = ex.getRequestURI().toString().split("/");
		int id = Integer.parseInt(uriSplit[uriSplit.length - 1]);
		System.out.println("[MOCK HUE]\treceived a light info request for lamp " + id);
		StringBuilder result = new StringBuilder();
		result.append('{');
		List<Integer> l = new ArrayList<>(guis.keySet());
		result.append("\"light_" + l.indexOf(id) + "\": {");
		result.append("\"id\": " + id + ",");
		result.append("\"on\": " + guis.get(id).isOn());
		result.append('}');
		result.append('}');
		byte [] response = result.toString().getBytes();
		ex.sendResponseHeaders(200, response.length);
		ex.getResponseHeaders().set("Content-Type", "application/json");
		ex.getResponseBody().write(response);
		ex.getResponseBody().close();
	}
	
	public void handleLights(HttpExchange ex) throws IOException {
		System.out.println("[MOCK HUE]\treceived a light info request");
		StringBuilder result = new StringBuilder();
		result.append('{');
		List<Integer> l = new ArrayList<>(guis.keySet());
		for(int i = 0; i < l.size(); i++) {
			result.append("\"light_" + i + "\": {");
			result.append("\"id\": " + l.get(i) + ",");
			result.append("\"on\": " + guis.get(l.get(i)).isOn());
			result.append('}');
			if(i < guis.size() - 1) 
				result.append(',');
		}
		result.append('}');
		byte [] response = result.toString().getBytes();
		ex.sendResponseHeaders(200, response.length);
		ex.getResponseHeaders().set("Content-Type", "application/json");
		ex.getResponseBody().write(response);
		ex.getResponseBody().close();
	}
	
	public void handleSwitch(HttpExchange ex) throws IOException {
		Scanner scanner = new Scanner(ex.getRequestBody());
		String[] values = scanner.nextLine().trim().replace("{", "").replace("}", "").split(":");
		scanner.close();
		String[] uriSplit = ex.getRequestURI().toString().split("/");
		int id = Integer.parseInt(uriSplit[uriSplit.length - 2]); 
		System.out.println("[MOCK HUE]\treceived a light toggle request for lamp " + id);
		
		if(!"on".equals(values[0].replace("\"", "").trim())) {
			System.out.println("[MOCK HUE]\tinvalid field " + values[0]);
			return;
		}
		
		boolean on = Boolean.parseBoolean(values[1].trim().replace("\"", ""));
		LedGui gui = guis.get(id);
		gui.toggle();
		if(!on && gui.isOn())
			gui.toggle();
		
		ex.sendResponseHeaders(200, 0);
		ex.getResponseBody().close();
	}
}
