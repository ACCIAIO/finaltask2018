System rapidash

/* ============================= EVENTS, MESSAGES ============================= */

/*
 * These dispatches are the ones (from the FRONTEND)
 * used to start (stop) the cleaning behaviour of the robot.
 * There's no need of arguments.
 */
Dispatch startCmd: startCmd				
Dispatch stopCmd: stopCmd

/*
 * These dispatches are the ones (from the CONTROLLER)
 * used to start (stop) the cleaning behaviour of the robot.
 * There's no need of arguments.
 */
Dispatch startCleaning: startCleaning
Dispatch stopCleaning: stopCleaning

/*
 * These dispatches are used exclusively by the
 * controller during the initialization procedure
 * to retrieve the current temperature and time.
 */
Dispatch tempRequest: tempRequest
Dispatch timeRequest: timeRequest

/*
 * Emitted by the clock and thermometer whenever the
 * registered hour or temperature changes. The new value 
 * V is then saved inside the KB of the controller.
 */
Event timeChange: timeChange(V)
Event tempChange: tempChange(V)

/*
 * Leds are the only actors receptive to this event. 
 * It toggle the blinking activity (S can be true or
 * false). 
 */
Event blink: blink(S)

/*
 * This is the only way to control the robot (physical or
 * virtual): it can contain w, a, s, d or h as specific 
 * commands to, respectively: go forward, go backward,
 * turn left, turn right and stop.
 */
Event cmd: cmd(C)

/*
 * Commands from the frontend shall be filtered by the mind
 * in order to ignore them whenever the cleaning behaviour
 * is being executed.
 */
Event mindCmd: cmd(C)

/*
 * These events come from sonars (onboard or not) and
 * are used to sense what happens to the robot when moving 
 * in its environment
 */
Event sonar1: sonar(D)
Event sonar2: sonar(D)
Event movingCollision: collision
Event fixedCollision: collision

/*
 * Changes in the model trigger a possible invalidation
 * process which ends by sending an event used by the
 * controller during the execution of the behaviour to
 * eventually terminate the execution itself.
 */
Dispatch invalid_m: invalid(T)
Event invalid: invalid(T)

/*
 * Commands from the frontend server. Usually this means 
 * a human sent a command from a UI.
 */
Event usercmd : usercmd(CMD)

/*
 * Used to update thresholds and opther constraints of the resource
 * model. The dispatch version is used to force an event-driven behaviour.
 * X is the new pair field, value es. update(temp_threshold(25))
 */
Event update : update(X)
Dispatch update_m : update(X)

/*
 * Used to communicate with the mapper. It contains information about 
 * the state of the tile at X, Y (a tile being a virtual measuring unit
 * for the map area.
 */
Event explored: explored(X, Y, V)

/*
 * HALL OF SHAME
 * All automessages
 */
Dispatch distanceOk: distanceOk
Dispatch wallsMappingCompleted: wallsMappingCompleted
Dispatch adjust: adjusted
Dispatch cleaningCompleted: cleaningCompleted
Dispatch next: next(C)

/* ================================= CONTEXTS ================================= */

/*
 * Contains the resource model controller, the mind
 * and the basic thermometer.
 */
Context ctxControl ip[host="localhost" port=9000]
 EventHandler invalid_handler for invalid {
	forwardEvent controller -m invalid_m
};

/*
 * Contains the clock, which could be anywhere in the
 * city.
 */
Context ctxSensors ip[host="localhost" port=9001]

/*
 * Contains the QActor bridge to/from the frontend
 * server.
 */
 Context ctxFrontend ip[host="localhost" port=9002] -httpserver
EventHandler update_handler for update {
	forwardEvent controller -m update_m
};
 
 /*
  * Soffritti's virtual robot context
  */
 Context virtual ip[host="localhost" port=9003] -standalone 
 
 /* ================================= CONTROL ================================= */
 
QActor controller context ctxControl {
	
	Rules {
		check_temp :- temp_threshold(T), model(temperature, V), eval(lt, V, T).
		check_time :- time_bounds(L, U), model(hour, H), eval(lt, H, U), eval(ge, H, L).
		
		changeModelAction(temperature, _) :- check_temp, !.
		changeModelAction(hour, _) :- check_time, !.
		changeModelAction(T, _) :- emitevent(invalid, invalid(T)).
	}
	
	Plan init normal
	[
		println("[CONTROLLER]\tstart");
		demo consult("model_engine.pl");
		demo consult("resource_model.pl");
		[!? temp_threshold(V)] javaRun it.unibo.frontend.modelClientHttp.updateResource(
			"sensors",
			"temperature",
			"ub",
			V);
		[!? time_bounds(L,_)] javaRun it.unibo.frontend.modelClientHttp.updateResource(
			"sensors",
			"clock",
			"lb",
			L);
		[!? time_bounds(_,U)] javaRun it.unibo.frontend.modelClientHttp.updateResource(
			"sensors",
			"clock",
			"ub",
			U);
		forward thermometer -m tempRequest: tempRequest;
		forward clock -m timeRequest: timeRequest
	]
	switchTo startWait
	
	Plan startWait
	[
		println("[CONTROLLER]\twaiting a start command")
	]
	transition stopAfter 864000
	whenEvent startCmd -> checkTemp,
	whenEvent tempChange -> updateTemp,
	whenEvent timeChange -> updateTime,
	whenMsg update_m -> updateConstraints,
	whenMsg invalid_m -> startWait
	finally repeatPlan
	
	Plan updateTemp resumeLastPlan
	[
		printCurrentEvent;
		onEvent tempChange: tempChange(V) -> demo changeModel(temperature, V);
		[!? model(temperature,V)] 
			javaRun it.unibo.frontend.modelClientHttp.updateResource(
			"sensors",
			"temperature",
			"value",
			V)
	]
	
	Plan updateTime resumeLastPlan
	[
		printCurrentEvent;
		onEvent timeChange: timeChange(V) -> demo changeModel(hour, V);
		[!? model(hour,V)] 
			javaRun it.unibo.frontend.modelClientHttp.updateResource(
			"sensors",
			"clock",
			"value",
			V)
	]
	
	Plan updateConstraints resumeLastPlan [
		printCurrentMessage;
		onMsg update_m : update(time_bounds(L,U)) ->
			demo replaceRule(time_bounds(_,_),time_bounds(L,U));
		onMsg update_m : update(temp_threshold(T)) ->
			demo replaceRule(temp_threshold(_),temp_threshold(T))
	]
	
	Plan checkTemp
	[
		// R-TempOk
		println("[CONTROLLER]\tChecks temperature constraint before starting the activity");
		[!? check_temp] println("[CONTROLLER]\ttemperature constraint ok")
		else { selfMsg invalid_m: invalid(temp); println("[CONTROLLER]\ttemperature constraint NOT ok") }
	]
	transition whenTime 500 -> checkTime
	whenMsg invalid_m -> startWait
	
	Plan checkTime
	[
		// R-TimeOk
		println("[CONTROLLER]\tChecks time constraint before starting the activity");
		[!? check_time] println("[CONTROLLER]\ttime constraint ok")
		else { selfMsg invalid_m : invalid(timeout); println("[CONTROLLER]\ttime constraint NOT ok") }
	]
	transition whenTime 500 -> waitForSonar1
	whenMsg invalid_m -> startWait
	
	Plan waitForSonar1
	[
		println("[CONTROLLER]\twaiting for sonar1")
	]
	transition whenTime 1000 -> startWait
	whenEvent sonar1 -> checkDistance
	
	Plan checkDistance
	[
		onEvent sonar1: sonar(X) -> demo replaceRule(distance(_), distance(X));
		[!? checkDistance] selfMsg distanceOk: distanceOk
	]
	transition whenTime 200 -> startWait
	whenMsg distanceOk -> sendStart
	
	Plan sendStart
	[
		println("[CONTROLLER]\tRUNNING ROBOT");
		forward mind -m startCleaning: startCleaning;
		emit blink: blink(true)
	]
	switchTo running
	
	Plan running
	[
		
	]
	transition whenTime 500 -> running
	whenEvent tempChange -> updateTemp,
	whenEvent timeChange -> updateTime,	
	whenMsg update_m -> updateConstraints,
	whenMsg invalid_m -> stopEverything,
	whenMsg cleaningCompleted -> stopEverything
	finally repeatPlan
	
	Plan stopEverything
	[
		println("[CONTROLLER]\tSTOPPING ROBOT");
		forward mind -m stopCleaning: stopCleaning;
		emit blink: blink(false)
	]
	switchTo startWait
}

QActor thermometer context ctxControl {
	Plan init normal
	[
		println("[THERMOMETER]\tstart")
	]	
	transition stopAfter 86400
	whenMsg tempRequest -> send
	
	Plan send 
	[
		println("[THERMOMETER]\tsending 23");
		emit tempChange: tempChange(23);
		delay 3000;
		println("[THERMOMETER]\tsending 21");
		emit tempChange: tempChange(21)
	]
}

QActor mapper context ctxControl {
	Plan init normal
	[
		javaRun it.unibo.mapper.mapping.init("9");
		javaRun it.unibo.mapper.mapping.initGui()
	]
	switchTo waiter
	
	Plan waiter
	[
		
	]
	transition stopAfter 640000000
	whenEvent explored -> build
	finally repeatPlan
	
	Plan build resumeLastPlan
	[
		onEvent explored: explored(X, Y, V) -> javaRun it.unibo.mapper.mapping.update(X, Y, V)
	]
	
}

QActor mind context ctxControl {
	Rules{
		robotStopped.	
	}
	
	Plan init normal
	[
		println("[MIND]\tstart");
		demo consult("mind_engine.pl")
	]
	switchTo waitingCommands
	
	Plan waitingCommands
	[
		removeRule msgIgnored;
		println("[MIND]\tWaiting commands.")
	]
	transition stopAfter 360000
		whenEvent mindCmd -> executeCommand,
		whenMsg startCleaning -> initRobotPosition,
		whenMsg stopCleaning  -> ignoreMessage //Ignoring useless messages
	finally repeatPlan
	
	Plan ignoreMessage resumeLastPlan
	[
		println("[MIND]\tI voluntarily ignored a message");
		addRule msgIgnored
	]
	
	Plan executeCommand resumeLastPlan
	[
		onEvent mindCmd: cmd(C) -> emit cmd: cmd(C)
	]
	
	Plan initRobotPosition
	[
		println("[MIND]\tSonar1 detected.");
		println("[MIND]\tInit Robot Position.");

		emit explored : explored (1,1,1);
		delay 200;
		emit cmd : cmd(a);
		delay 200;
		emit explored : explored (0,1,2);
		delay 2000;
		emit cmd : cmd(a);
		addRule moving("down");
		addRule trying("left");
		addRule head("down");
		addRule position(1,1);
		addRule firstTimeIn11;
		
		println("[MIND]\tStarting walls mapping.");
		
		[!? position(X,_)] println(X);
		[!? position(_,Y)] println(Y)
	]
	switchTo identifyingRoomBorder
	
	// movimento in avanti
	Plan identifyingRoomBorder
	[	
		[not !? msgIgnored ]{
			println("[MIND]\tIdentifying Room, moving forward.");
			
			[!? position(1,1)]{
				[?? firstTimeIn11]{
					println("[MIND]\t First Time in position (1,1).");
					delay 1000;
					emit cmd : cmd(w)
				}
				else
					selfMsg wallsMappingCompleted : wallsMappingCompleted			
			}else{
				delay 1000;
				emit cmd : cmd(w)
			}			
		} else
			removeRule msgIgnored
	]
	transition whenTime 1000 -> searchOpening
		whenEvent sonar2 -> sonar2Found,
		whenMsg wallsMappingCompleted -> initCleanRoomCenter,
		whenMsg stopCleaning -> stopCleaning,
		whenMsg startCleaning -> ignoreMessage,
		whenEvent fixedCollision -> changeDirection,
		whenEvent movingCollision -> avoidMovingObstacle
	finally repeatPlan
	
	Plan avoidMovingObstacle
	[
		println("[MIND]\tAvoiding mobile obstacle.");
		delay 500
	]
	transition whenTime 500 -> doForward
		whenEvent movingCollision -> avoidMovingObstacle
	
	Plan doForward
	[
		println("[MIND]\tDo forward.");
		emit cmd : cmd(w)
	]
	transition whenTime 1000 -> searchOpening
		whenEvent movingCollision -> avoidMovingObstacle
	
	Plan identifyingRoomBorderAfterSonar
	[	
		[not !? msgIgnored ]{
			println("[MIND]\tIdentifying Room, moving forward.");
			
			[!? position(1,1)]{
				[?? firstTimeIn11]{
					println("[MIND]\t First Time in position (1,1).");
					delay 1000;
					emit cmd : cmd(w)
				}
				else
					selfMsg wallsMappingCompleted : wallsMappingCompleted			
			}else{
				delay 1000;
				emit cmd : cmd(w)
			}			
		} else
			removeRule msgIgnored
	]
	transition whenTime 1000 -> searchOpening
		whenEvent fixedCollision -> changeDirection,
		whenMsg wallsMappingCompleted -> initCleanRoomCenter,
		whenMsg stopCleaning -> stopCleaning,
		whenMsg startCleaning -> ignoreMessage
	finally repeatPlan
	
	Plan searchOpening
	[
		println("[MIND]\tSearching Openings into walls.");
		demo updateX;
		demo updateY;
		[!? position(X,_)] println(X);
		[!? position(_,Y)] println(Y);
		[!? position(X,Y)] emit explored : explored(X,Y,1);
		delay 500;
		
		[!? trying("left")]{
			println("[MIND]\t trying LEFT.");
			[!? head("up")]
				emit cmd : cmd(a);
			[!? head("down")]
				emit cmd : cmd(d);
			[!? head("right")] {	
				emit cmd : cmd(a);
				delay 1000;
				emit cmd : cmd(a)
			};
			removeRule head(_);
			addRule head("left")
		};
		[!? trying("right")]{
			println("[MIND]\t trying RIGHT.");
			[!? head("up")]
				emit cmd : cmd(d);
			[!? head("down")]
				emit cmd : cmd(a);
			[!? head("left")] {	
				emit cmd : cmd(a);
				delay 1000;
				emit cmd : cmd(a)
			};
			removeRule head(_);
			addRule head("right")
		};
		[!? trying("up")]{
			println("[MIND]\t trying UP.");
			[!? head("left")]
				emit cmd : cmd(d);
			[!? head("right")]
				emit cmd : cmd(a);
			[!? head("down")] {	
				emit cmd : cmd(a);
				delay 1000;
				emit cmd : cmd(a)
			};
			removeRule head(_);
			addRule head("up")
		};
		[!? trying("down")]{
			println("[MIND]\t trying DOWN.");
			[!? head("left")]
				emit cmd : cmd(a);
			[!? head("right")]
				emit cmd : cmd(d);
			[!? head("down")] {	
				emit cmd : cmd(a);
				delay 1000;
				emit cmd : cmd(a)
			};
			removeRule head(_);
			addRule head("down")
		}				
	]
	transition whenTime 1000 -> moveForward
		whenEvent fixedCollision -> restoreHeadDirection
	
	Plan doForwardAfterOpening
	[
		println("[MIND]\tDo forward after opening.");
		delay 500
	]
	transition whenTime 500 -> moveForward
		whenEvent movingCollision -> doForwardAfterOpening
	
	Plan moveForward
	[
		println("[MIND]\tMove forward.");
		emit cmd : cmd(w);
		delay 190;
		emit cmd : cmd(h)
	]
	transition whenTime 1000 -> changingDirectionForOpening
		whenEvent movingCollision -> doForwardAfterOpening, 
		whenEvent fixedCollision -> restoreHeadDirection
	
	Plan changingDirectionForOpening
	[
		addRule openingDetected
	]
	switchTo changeDirection
	
	// ripristino direzione di marcia
	Plan restoreHeadDirection
	[
		delay 200;
		println("[MIND]\tRestore Head Direction.");
		[!? nextPosition(X,Y)]
			emit explored : explored(X,Y,2);
		[!? moving("left")]{
			println("[MIND]\t Moving LEFT.");
			[!? head("up")]
				emit cmd : cmd(a);
			[!? head("down")]
				emit cmd : cmd(d);
			[!? head("right")] {	
				emit cmd : cmd(a);
				delay 1000;
				emit cmd : cmd(a)
			};
			removeRule head(_);
			addRule head("left")
		};
		[!? moving("right")]{
			println("[MIND]\t Moving RIGHT.");
			[!? head("up")]
				emit cmd : cmd(d);
			[!? head("down")]
				emit cmd : cmd(a);
			[!? head("left")] {	
				emit cmd : cmd(a);
				delay 1000;
				emit cmd : cmd(a)
			};
			removeRule head(_);
			addRule head("right")
		};
		[!? moving("up")]{
			println("[MIND]\t Moving UP.");
			[!? head("left")]
				emit cmd : cmd(d);
			[!? head("right")]
				emit cmd : cmd(a);
			[!? head("down")] {	
				emit cmd : cmd(a);
				delay 1000;
				emit cmd : cmd(a)
			};
			removeRule head(_);
			addRule head("up")
		};
		[!? moving("down")]{
			println("[MIND]\t Moving DOWN.");
			[!? head("left")]
				emit cmd : cmd(a);
			[!? head("right")]
				emit cmd : cmd(d);
			[!? head("down")] {	
				emit cmd : cmd(a);
				delay 1000;
				emit cmd : cmd(a)
			};
			removeRule head(_);
			addRule head("down")
		};
		delay 500
	]
	switchTo identifyingRoomBorder
	
	Plan changeDirection
	[
		println("[MIND]\tChange Robot Direction.");
		[not !? openingDetected]{
			[!? nextPosition(X,Y)] emit explored : explored(X,Y,2)
		};
		[!? nextPosition(X,Y)] javaRun it.unibo.mapper.mapping.adjustMovement(X,Y);
		
		[!? position(X,Y)] emit explored : explored(X,Y,1);
		// le condizioni sono mutuamente esclusive ma dentro ognuna i valori vengono modificati, per cui c'� necessit� degli else.
		// (1) -> (2)
		[!? wallDetected("left","left")]{
			println("[MIND]\tLL wall");
			removeRule moving(_);
			removeRule trying(_);
			removeRule head(_);
			
			addRule moving("down");
			addRule trying("left");
			addRule head("down");
			emit cmd : cmd(a)
		} else {
			// (2a) -> (3)
			[!? wallDetected("down","left")]{
				println("[MIND]\tDL wall");
				removeRule moving(_);
				removeRule trying(_);
				removeRule head(_);
				
				addRule moving("right");
				addRule trying("down");
				addRule head("right");
				emit cmd : cmd(a)
			} else {
				// (2b) -> (4)
				[!? openingDetected("down","left")]{
					println("[MIND]\tDL opening");
					removeRule moving(_);
					removeRule trying(_);
					
					addRule moving("left");
					addRule trying("up")
				} else{
					// (4a) -> (2)
					[!? wallDetected("left","up")]{
						println("[MIND]\tLU wall");
						removeRule moving(_);
						removeRule trying(_);
						removeRule head(_);
						
						addRule moving("down");
						addRule trying("left");
						addRule head("down");
						emit cmd : cmd(a)
					} else {
						// (4b) -> (5)
						[!? openingDetected("left","up")]{
							println("[MIND]\tLU opening");
							removeRule moving(_);
							removeRule trying(_);
							
							addRule moving("up");
							addRule trying("right")
						} else {
							// (3a) -> (5)
							[!? wallDetected("right","down")]{
								println("[MIND]\tRD wall");
								removeRule moving(_);
								removeRule trying(_);
								removeRule head(_);
								
								addRule moving("up");
								addRule trying("right");
								addRule head("up");
								emit cmd : cmd(a)
							} else {
								// (3b) -> (2)
								[!? openingDetected("right","down")]{
									println("[MIND]\tRD opening");
									println("[MIND]\tOpeningDetected Rigth - Down");
									removeRule moving(_);
									removeRule trying(_);
									
									addRule moving("down");
									addRule trying("left")
								} else{
									// (5a) -> (4)
									[!? wallDetected("up","right")]{
										println("[MIND]\tUR wall");
										removeRule moving(_);
										removeRule trying(_);
										removeRule head(_);
										
										addRule moving("left");
										addRule trying("up");
										addRule head("left");
										emit cmd : cmd(a)
									} else {
										// (5b) -> (3)
										[!? openingDetected("up","right")]{
											println("[MIND]\tUR opening");
											removeRule moving(_);
											removeRule trying(_);
											
											addRule moving("right");
											addRule trying("down")
										}	
									}
								}
							}
						}
					}	
				}
			}
		};
		[?? openingDetected]{
			demo updateX;
			demo updateY;
			[!? position(X,_)] println(X);
			[!? position(_,Y)] println(Y);
			[!? position(X,Y)] emit explored : explored(X,Y,1)
		}
	]
	transition whenTime 1000 -> identifyingRoomBorder
		whenMsg adjust -> updatePositionAfterChangeDirection
	
	Plan updatePositionAfterChangeDirection
	[
		[!? nextPosition(X,Y)]
			demo replacePosition(X,Y)
	]
	switchTo identifyingRoomBorder
	
	Plan sonar2Found
	[
		addRule sonar2Found;
		addRule msgIgnored
	]
	switchTo identifyingRoomBorderAfterSonar
	
	Plan initCleanRoomCenter
	[
		println("[MIND]\tWalls mapping completed.");
		println("[MIND]\tStarting cleaning room center.");
		
		[!? sonar2Found]{
			println("[MIND]\tSonar2 found.");
					
		delay 500;
		[!? directions(X,Y,Z)] println(X);
		[!? directions(X,Y,Z)] println(Y);
		[!? directions(X,Y,Z)] println(Z);
		
		//emit cmd : cmd(w);
		//delay 50;
		//emit cmd : cmd(h);
		delay 500	
			
		} else{
			println("[MIND]\tMapping walls completed but sonar2 not found. Stopping.");
			selfMsg stopCleaning : cleaningStop
		}
	]
	transition whenTime 2000 -> cleanCenter
		whenMsg stopCleaning -> stopCleaning
	
	Plan cleanCenter
	[
		[not !? msgIgnored]{
			[!? positionAndDirection(X,Y,Z)]
				javaRun it.unibo.mapper.mapping.next(X,Y,Z);
			println("[MIND]\tNext piece requested.")
		} else
			removeRule msgIgnored
	]
	transition stopAfter 36500
		whenEvent movingCollision -> waitForMovingObstacleAfterCleanCenter,
		whenMsg next -> cleaning,
		whenMsg stopCleaning -> stopCleaning,
		whenMsg startCleaning -> ignoreMessage
	finally repeatPlan
	
	Plan waitForMovingObstacleAfterCleanCenter 
	[
		println("[MIND]\tWait for moving obstacle.")
	]
	transition whenTime 1000 -> cleaning
		whenEvent movingCollision -> waitForMovingObstacleAfterCleanCenter
	
	Plan cleaning 
	[
		println("[MIND]\tCleaning.");
		
		[!? positionAndDirection(X,Y,Z)] println(X);
		[!? positionAndDirection(X,Y,Z)] println(Y);
		[!? positionAndDirection(X,Y,Z)] println(Z);		
		
		onMsg next : next(X) -> addRule commandReceived(X);
		[!? commandReceived(X)]
			println(X);
		
		[?? commandReceived("w")]{
			emit cmd : cmd(w);
			addRule moved
		};
		[?? commandReceived("s")]{
			emit cmd : cmd(s);
			addRule moved
		};
		[?? commandReceived("a")]{
				emit cmd : cmd(a);
				[?? head("right")]{
					addRule head("up");
					removeRule moving(_);
					addRule moving("up")
				}
				else{
					[?? head("down")]{
						addRule head("right");
						removeRule moving(_);
						addRule moving("right")
					};
					[?? head("left")]{
						addRule head("down");
						removeRule moving(_);
						addRule moving("down")
					};
					[?? head("up")]{
						addRule head("left");
						removeRule moving(_);
						addRule moving("left")
					}
				}
		};
		[?? commandReceived("d")]{
			emit cmd : cmd(d);
			[?? head("right")]{
					addRule head("down");
					removeRule moving(_);
					addRule moving("down")
			} else {
				[?? head("up")]{
					addRule head("right");
					removeRule moving(_);
					addRule moving("right")
				};
				[?? head("left")]{
					addRule head("up");
					removeRule moving(_);
					addRule moving("up")
				};
				[?? head("down")]{
					addRule head("left");
					removeRule moving(_);
					addRule moving("left")
				}
			}
		};
		[?? commandReceived("h")]
			selfMsg cleaningCompleted : cleaningCompleted		
	]
	transition whenTime 2000 -> pieceCleaned
		whenMsg cleaningCompleted -> ending,
		whenEvent fixedCollision -> unreachablePosition
	
	Plan pieceCleaned
	[
		println("[MIND]\tPiece Cleaned.");
		[?? moved]{
			demo updateX;
			demo updateY;
			[!? position(X,Y)]
				emit explored : explored(X,Y,1)
		}
	]
	switchTo cleanCenter
	
	Plan unreachablePosition
	[
		println("[MIND]\tUnreachable Position.");
		removeRule moved;
		[!? nextPosition(X,Y)]
			emit explored : explored(X,Y,2);
		[!? nextPosition(X,Y)]
			javaRun it.unibo.mapper.mapping.adjustMovement(X,Y)
	]
	transition whenTime 500 -> cleanCenter
		whenMsg adjust -> updatePositionAfterUnreachablePosition
		
	Plan updatePositionAfterUnreachablePosition
	[
		[!? nextPosition(X,Y)]
			demo replacePosition(X,Y)		
	]
	switchTo cleanCenter
	
	Plan ending
	[
		println("[MIND]\tSonar2 reached.");
		forward controller -m cleaningCompleted: cleaningCompleted;
		javaRun it.unibo.mapper.mapping.mappingEnd()
	]
	switchTo stopCleaning
	
	Plan stopCleaning
	[
		println("[MIND]\tStopping the cleaning of the room.");
		removeRule robotStarted;
		addRule robotStopped
	]
	switchTo waitingCommands
	
}

/* ================================= SENSORS ================================= */

QActor clock context ctxSensors {
	Plan init normal
	[
		println("[CLOCK]\tstart");
		javaRun it.unibo.control.utils.clockUtils.register()
	]
	switchTo waitForMessages
	
	Plan waitForMessages
	[
		
	]
	transition whenTime 36000000 -> waitForMessages
	whenMsg timeRequest -> handleTimeRequest
	finally repeatPlan
	
	Plan handleTimeRequest resumeLastPlan
	[
		delay 2000;
		onMsg timeRequest: timeRequest -> javaRun it.unibo.control.utils.clockUtils.forceEmit();
		println("[CLOCK]\tforced a timeChanged")
	]
	
}

/* ================================ FRONTEND ================================ */

QActor frontendbridge context ctxFrontend {
	Plan init normal [
		println("[FRONTENDBRIDGE]\tfrontend bridge start")
	]
	transition stopAfter 864000
	whenEvent usercmd -> loop
	
	Plan loop [
		printCurrentEvent;
		onEvent usercmd : usercmd(robotgui(startcmd)) ->
			forward controller -m startCmd : startCmd;
		onEvent usercmd : usercmd(robotgui(stopcmd)) ->
			forward controller -m stopCmd : stopCmd;
		onEvent usercmd: usercmd(robotgui(w)) -> emit mindCmd: cmd(w);
		onEvent usercmd: usercmd(robotgui(a)) -> emit mindCmd: cmd(a);
		onEvent usercmd: usercmd(robotgui(s)) -> emit mindCmd: cmd(s);
		onEvent usercmd: usercmd(robotgui(d)) -> emit mindCmd: cmd(d);
		onEvent usercmd: usercmd(robotgui(h)) -> emit mindCmd: cmd(h)
	]
	transition stopAfter 864000
	whenEvent usercmd -> loop
}

