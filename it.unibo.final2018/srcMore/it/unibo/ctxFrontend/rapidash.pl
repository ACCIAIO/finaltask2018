%====================================================================================
% Context ctxFrontend  SYSTEM-configuration: file it.unibo.ctxFrontend.rapidash.pl 
%====================================================================================
context(ctxcontrol, "localhost",  "TCP", "9000" ).  		 
context(ctxsensors, "localhost",  "TCP", "9001" ).  		 
context(ctxfrontend, "localhost",  "TCP", "9002" ).  		 
context(virtual, "localhost",  "TCP", "9003" ).  		 
%%% -------------------------------------------
qactor( controller , ctxcontrol, "it.unibo.controller.MsgHandle_Controller"   ). %%store msgs 
qactor( controller_ctrl , ctxcontrol, "it.unibo.controller.Controller"   ). %%control-driven 
qactor( thermometer , ctxcontrol, "it.unibo.thermometer.MsgHandle_Thermometer"   ). %%store msgs 
qactor( thermometer_ctrl , ctxcontrol, "it.unibo.thermometer.Thermometer"   ). %%control-driven 
qactor( mapper , ctxcontrol, "it.unibo.mapper.MsgHandle_Mapper"   ). %%store msgs 
qactor( mapper_ctrl , ctxcontrol, "it.unibo.mapper.Mapper"   ). %%control-driven 
qactor( mind , ctxcontrol, "it.unibo.mind.MsgHandle_Mind"   ). %%store msgs 
qactor( mind_ctrl , ctxcontrol, "it.unibo.mind.Mind"   ). %%control-driven 
qactor( clock , ctxsensors, "it.unibo.clock.MsgHandle_Clock"   ). %%store msgs 
qactor( clock_ctrl , ctxsensors, "it.unibo.clock.Clock"   ). %%control-driven 
qactor( frontendbridge , ctxfrontend, "it.unibo.frontendbridge.MsgHandle_Frontendbridge"   ). %%store msgs 
qactor( frontendbridge_ctrl , ctxfrontend, "it.unibo.frontendbridge.Frontendbridge"   ). %%control-driven 
%%% -------------------------------------------
eventhandler(update_handler,ctxfrontend,"it.unibo.ctxFrontend.Update_handler","update").  
%%% -------------------------------------------

