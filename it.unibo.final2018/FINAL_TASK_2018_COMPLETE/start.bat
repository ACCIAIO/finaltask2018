start cmd /c java -jar mockhue.jar

cd frontend
start cmd /c java -jar it.unibo.final2018-1.0.jar

cd ..\nodecode\auth
start cmd /c node app.js

cd ..\..\virtual_bot
start cmd /c startServer.bat

timeout 4

cd ..\virtual
start cmd /c java -jar it.unibo.final2018.virtual-1.0.jar

timeout 3

cd ..\sensors
start cmd /c java -jar it.unibo.final2018-1.0.jar

timeout 3

cd ..\control
start cmd /c java -jar it.unibo.final2018-1.0.jar