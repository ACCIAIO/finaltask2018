% Mapping rules

updateX :- 	moving("right"),
			!, 
			position(X, Y),
			NEWX is X+1,
			replaceRule(position(X, Y), position(NEWX, Y)).

updateX :- 	moving("left"),
			!,
			position(X, Y),
			NEWX is X-1,
			replaceRule(position(X, Y), position(NEWX, Y)).

updateY :- 	moving("down"),
			!, 
			position(X, Y),
			NEWY is Y+1,
			replaceRule(position(X, Y), position(X, NEWY)).

updateY :- 	moving("up"),
			!,
			position(X, Y),
			NEWY is Y-1,
			replaceRule(position(X, Y), position(X, NEWY)).

nextPosition(NEWX, NEWY) :- 	head("right"),
								!,
								position(X, Y),
								NEWX is X+1,
								NEWY is Y.


nextPosition(NEWX, NEWY) :- 	head("left"),
								!,
								position(X, Y),
								NEWX is X-1,
								NEWY is Y.

nextPosition(NEWX, NEWY) :- 	head("down"),
								!,
								position(X, Y),
								NEWX is X,
								NEWY is Y+1.

nextPosition(NEWX, NEWY) :- 	head("up"),
								!,
								position(X, Y),
								NEWX is X,
								NEWY is Y-1.

rotateHead(X, "a") :- head("up"), !, X is "left".
rotateHead(X, "a") :- head("left"), !, X is "down".
rotateHead(X, "a") :- head("down"), !, X is "right".
rotateHead(X, "a") :- head("right"), !, X is "up".

rotateHead(X, "d") :- head("up"), !, X is "right".
rotateHead(X, "d") :- head("left"), !, X is "up".
rotateHead(X, "d") :- head("down"), !, X is "left".
rotateHead(X, "d") :- head("right"), !, X is "down".

directions(X,Y,Z) :- moving(X), trying(Y), head(Z).
positionAndDirection(X,Y,Z) :- position(X,Y), head(Z).
wallDetected(X,Y) :- directions(X,Y,X).
openingDetected(X,Y) :- directions(X,Y,Y).
replacePosition(X,Y) :- replaceRule(position(_,_), position(X,Y)).

emitevent(EVID, EVCONTENT) :- actorobj(Actor), Actor <- emit(EVID, EVCONTENT).