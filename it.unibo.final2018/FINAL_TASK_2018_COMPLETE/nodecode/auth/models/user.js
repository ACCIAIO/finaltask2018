var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var UserSchema = new mongoose.Schema({
	username: {
		type: String,
		unique: true,
		required: true,
		trim: true
	},
	password: {
		type: String,
		required: true,
	},
	name: {
		type: String,
		required: true,
		trim: true
	},
	last_name: {
		type: String,
		required: true,
		trim: true
	},
	email: {
		type: String,
		unique: true,
		required: false,
	},
	role: {
		type: String,
		required: false
	},
	bio: {
		type: String,
		required: false,
		trim: true
	},
	admin: {
		type: Boolean,
		default: false
	}
});

//Hashing a pwd before saving it to the db
UserSchema.pre('save', function(next){
	var user = this;
	bcrypt.hash(user.password, 10, function(err, hash){
		if(err){
			return next(err);
		}
		user.password = hash;
		next();
	})
});

//Authenticate input against db
UserSchema.statics.authenticate = function(username, password, callback){
	User.findOne({ username: username}).exec(function(err, user){
		if(err){
			return callback(err);
		}else if(!user){
			var err = new Error('User not found.');
			err.status = 401;
			return callback(err);
		}
		bcrypt.compare(password, user.password, function(err, result){
			if(result === true){
				return callback(null, user);
			} else{
				return callback();
			}
		});
	});
}

var User = mongoose.model('User', UserSchema);
module.exports = User;