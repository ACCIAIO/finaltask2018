var express = require('express');
var router = express.Router();
var resources = require('../models/resources');

/* Command */
router.get('/', function(req, res){
	if(req.session.user){		
		res.render('command', {title: 'Command', user: req.session.user, resources : resources});
	}else{
		res.redirect('/login?destination=command');
	}
});

module.exports = router;
