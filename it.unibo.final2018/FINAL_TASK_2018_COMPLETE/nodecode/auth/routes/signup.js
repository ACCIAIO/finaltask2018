var express = require('express');
var router = express.Router();

var User = require('../models/user');

var mailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/i;

/* POST signup*/
router.post('/reqSignUp', function(req, res, next){
	console.log('Richiesta di sign-up: '+req.body.username+", pwd: "+req.body.pwd);
	if(req.body.username &&
			req.body.pwd &&
			req.body.pwdConf &&
			req.body.name &&
			req.body.last_name){
		
		var userData = {
				username: req.body.username,
				password: req.body.pwd,
				name: req.body.name,
				last_name: req.body.last_name
		}
		
		
		if(req.body.email){
			if(mailRegex.test(req.body.email)){
				userData.email = req.body.email.toLowerCase();				
			}
			else{
				var err = new Error('Invalid email address: ' + req.body.email);
				err.status = 401;
				return next(err);
			}
		}
		
		if(req.body.role){
			userData.role = req.body.role;
		}
		
		if(req.body.bio){
			userData.bio = req.body.bio;
		}
		
		if(req.body.pwd !== req.body.pwdConf){
			console.log('SignUp Error: Mismatch between pwd and pwdConf.');
			var err = new Error('Password and Confirm Password fields shall be equal.');
			err.status = 401;
			return next(err);
		}
		
		//Use schema.create to insert data into the db
		User.create(userData, function(err, user){
			if(err){
				return next(err);
			}else{
				return res.redirect('/signup/success');
			}
		});
	}
});

/* Sign-up page */
router.get('/', function(req, res){
	console.log('Get di signup raggiunta');
	var user;
	if(req.session.user){
		user = req.session.user;
	}
	res.render('signup', {title: 'Sign-up', user: user});
});

/* GET post sign-up page */
router.get('/success', function(req, res){
	console.log('Iscrizione avvenuta con successo');
	var user;
	if(req.session.user){
		user = req.session.user;
	}
	res.render('signup_success', {title: 'Iscrizione completa', user: user});
});

module.exports = router;
