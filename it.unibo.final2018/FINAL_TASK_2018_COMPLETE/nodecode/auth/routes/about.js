var express = require('express');
var router = express.Router();

/* GET about. */
router.get('/', function(req, res) {
	res.render('about', {title: "About", user: req.session.user});
});

module.exports = router;
