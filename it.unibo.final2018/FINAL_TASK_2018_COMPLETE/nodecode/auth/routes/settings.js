var express = 	require('express');
var router = 	express.Router();
var resources = require('../models/resources');
var robot = 	require('../models/robot');

/* GET system settings. */
router.get('/', function(req, res, next) {
	res.render('settings', {title: 'Robot Status', user: req.session.user, robot : robot, resources : resources});
});

/* GET to different urls update different parts of the model. */
router.get('/resources', function (req, res, next) {
	// Aggiorna il modello
	resources[req.query.type][req.query.id][req.query.field] = req.query.value;
	// req.result = "LED " + req.params.id + "= " + selectedLed.value; TODO
	res.redirect('/command');
});
router.get('/robot/sonar', function(req,res,next) {
	// Aggiorna il modello
	robot.sonar[req.query.id].value = req.query.value;
	res.redirect('/settings');
});

module.exports = router;
