var express = require('express');
var router = express.Router();

/* GET page still in the works. */
router.get('/', function(req, res, next) {
	if (req.session.user) {
		res.render('welcome', { title: 'Welcome', user: req.session.user});
	} else {
		res.redirect('/login');
	}
});

module.exports = router;
