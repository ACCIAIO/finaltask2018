var mailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/i;

var correctColor = "#dcffd5"; //Verde chiaro
var wrongColor = "#ffb0b0"; // Rosso chiaro

function validateEmail(){
	var emailField = document.getElementById('email');
	
	if(mailRegex.test(emailField.value)){
		emailField.style.backgroundColor = correctColor;
	}else{
		emailField.style.backgroundColor = wrongColor;
	}
}

function checkPwdConf(){
	
	var pwdField = document.getElementById('pwd');
	var pwdConfField = document.getElementById('pwdConf');
	
	if(pwdField.value === pwdConfField.value){
		pwdConfField.style.backgroundColor = correctColor;
	}else{
		pwdConfField.style.backgroundColor = wrongColor;
	}
}