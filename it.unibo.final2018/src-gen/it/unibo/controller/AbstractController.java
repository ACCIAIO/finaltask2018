/* Generated by AN DISI Unibo */ 
package it.unibo.controller;
import it.unibo.qactors.PlanRepeat;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.StateExecMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.action.AsynchActionResult;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.IActorAction.ActionExecMode;
import it.unibo.qactors.action.IMsgQueue;
import it.unibo.qactors.akka.QActor;
import it.unibo.qactors.StateFun;
import java.util.Stack;
import java.util.Hashtable;
import java.util.concurrent.Callable;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.qactors.action.ActorTimedAction;
public abstract class AbstractController extends QActor { 
	protected AsynchActionResult aar = null;
	protected boolean actionResult = true;
	protected alice.tuprolog.SolveInfo sol;
	protected String planFilePath    = null;
	protected String terminationEvId = "default";
	protected String parg="";
	protected boolean bres=false;
	protected IActorAction action;
	 
	
		protected static IOutputEnvView setTheEnv(IOutputEnvView outEnvView ){
			return outEnvView;
		}
		public AbstractController(String actorId, QActorContext myCtx, IOutputEnvView outEnvView )  throws Exception{
			super(actorId, myCtx,  
			"./srcMore/it/unibo/controller/WorldTheory.pl",
			setTheEnv( outEnvView )  , "init");
			this.planFilePath = "./srcMore/it/unibo/controller/plans.txt";
	  	}
		@Override
		protected void doJob() throws Exception {
			String name  = getName().replace("_ctrl", "");
			mysupport = (IMsgQueue) QActorUtils.getQActor( name ); 
			initStateTable(); 
	 		initSensorSystem();
	 		history.push(stateTab.get( "init" ));
	  	 	autoSendStateExecMsg();
	  		//QActorContext.terminateQActorSystem(this);//todo
		} 	
		/* 
		* ------------------------------------------------------------
		* PLANS
		* ------------------------------------------------------------
		*/    
	    //genAkkaMshHandleStructure
	    protected void initStateTable(){  	
	    	stateTab.put("handleToutBuiltIn",handleToutBuiltIn);
	    	stateTab.put("init",init);
	    	stateTab.put("startWait",startWait);
	    	stateTab.put("updateTemp",updateTemp);
	    	stateTab.put("updateTime",updateTime);
	    	stateTab.put("updateConstraints",updateConstraints);
	    	stateTab.put("checkTemp",checkTemp);
	    	stateTab.put("checkTime",checkTime);
	    	stateTab.put("waitForSonar1",waitForSonar1);
	    	stateTab.put("checkDistance",checkDistance);
	    	stateTab.put("sendStart",sendStart);
	    	stateTab.put("running",running);
	    	stateTab.put("stopEverything",stopEverything);
	    }
	    StateFun handleToutBuiltIn = () -> {	
	    	try{	
	    		PlanRepeat pr = PlanRepeat.setUp("handleTout",-1);
	    		String myselfName = "handleToutBuiltIn";  
	    		println( "controller tout : stops");  
	    		repeatPlanNoTransition(pr,myselfName,"application_"+myselfName,false,false);
	    	}catch(Exception e_handleToutBuiltIn){  
	    		println( getName() + " plan=handleToutBuiltIn WARNING:" + e_handleToutBuiltIn.getMessage() );
	    		QActorContext.terminateQActorSystem(this); 
	    	}
	    };//handleToutBuiltIn
	    
	    StateFun init = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("init",-1);
	    	String myselfName = "init";  
	    	temporaryStr = "\"[CONTROLLER]	start\"";
	    	println( temporaryStr );  
	    	parg = "consult(\"model_engine.pl\")";
	    	//QActorUtils.solveGoal(myself,parg,pengine );  //sets currentActionResult		
	    	solveGoal( parg ); //sept2017
	    	parg = "consult(\"resource_model.pl\")";
	    	//QActorUtils.solveGoal(myself,parg,pengine );  //sets currentActionResult		
	    	solveGoal( parg ); //sept2017
	    	if( (guardVars = QActorUtils.evalTheGuard(this, " !?temp_threshold(V)" )) != null ){
	    	it.unibo.frontend.modelClientHttp.updateResource( myself ,"sensors", "temperature", "ub", guardVars.get("V")  );
	    	}
	    	if( (guardVars = QActorUtils.evalTheGuard(this, " !?time_bounds(L,_)" )) != null ){
	    	it.unibo.frontend.modelClientHttp.updateResource( myself ,"sensors", "clock", "lb", guardVars.get("L")  );
	    	}
	    	if( (guardVars = QActorUtils.evalTheGuard(this, " !?time_bounds(_,U)" )) != null ){
	    	it.unibo.frontend.modelClientHttp.updateResource( myself ,"sensors", "clock", "ub", guardVars.get("U")  );
	    	}
	    	temporaryStr = QActorUtils.unifyMsgContent(pengine,"tempRequest","tempRequest", guardVars ).toString();
	    	sendMsg("tempRequest","thermometer", QActorContext.dispatch, temporaryStr ); 
	    	temporaryStr = QActorUtils.unifyMsgContent(pengine,"timeRequest","timeRequest", guardVars ).toString();
	    	sendMsg("timeRequest","clock", QActorContext.dispatch, temporaryStr ); 
	    	//switchTo startWait
	        switchToPlanAsNextState(pr, myselfName, "controller_"+myselfName, 
	              "startWait",false, false, null); 
	    }catch(Exception e_init){  
	    	 println( getName() + " plan=init WARNING:" + e_init.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//init
	    
	    StateFun startWait = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp(getName()+"_startWait",0);
	     pr.incNumIter(); 	
	    	String myselfName = "startWait";  
	    	temporaryStr = "\"[CONTROLLER]	waiting a start command\"";
	    	println( temporaryStr );  
	    	//bbb
	     msgTransition( pr,myselfName,"controller_"+myselfName,false,
	          new StateFun[]{stateTab.get("checkTemp"), stateTab.get("updateTemp"), stateTab.get("updateTime"), stateTab.get("updateConstraints"), stateTab.get("startWait") }, 
	          new String[]{"true","E","startCmd", "true","E","tempChange", "true","E","timeChange", "true","M","update_m", "true","M","invalid_m" },
	          864000, "handleToutBuiltIn" );//msgTransition
	    }catch(Exception e_startWait){  
	    	 println( getName() + " plan=startWait WARNING:" + e_startWait.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//startWait
	    
	    StateFun updateTemp = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("updateTemp",-1);
	    	String myselfName = "updateTemp";  
	    	printCurrentEvent(false);
	    	//onEvent 
	    	setCurrentMsgFromStore(); 
	    	curT = Term.createTerm("tempChange(V)");
	    	if( currentEvent != null && currentEvent.getEventId().equals("tempChange") && 
	    		pengine.unify(curT, Term.createTerm("tempChange(V)")) && 
	    		pengine.unify(curT, Term.createTerm( currentEvent.getMsg() ) )){ 
	    			String parg="changeModel(temperature,V)";
	    			/* PHead */
	    			parg =  updateVars( Term.createTerm("tempChange(V)"), 
	    			                    Term.createTerm("tempChange(V)"), 
	    				    		  	Term.createTerm(currentEvent.getMsg()), parg);
	    				if( parg != null ) {
	    				    aar = QActorUtils.solveGoal(this,myCtx,pengine,parg,"",outEnvView,86400000);
	    					//println(getName() + " plan " + curPlanInExec  +  " interrupted=" + aar.getInterrupted() + " action goon="+aar.getGoon());
	    					if( aar.getInterrupted() ){
	    						curPlanInExec   = "updateTemp";
	    						if( aar.getTimeRemained() <= 0 ) addRule("tout(demo,"+getName()+")");
	    						if( ! aar.getGoon() ) return ;
	    					} 			
	    					if( aar.getResult().equals("failure")){
	    						if( ! aar.getGoon() ) return ;
	    					}else if( ! aar.getGoon() ) return ;
	    				}
	    	}
	    	if( (guardVars = QActorUtils.evalTheGuard(this, " !?model(temperature,V)" )) != null ){
	    	it.unibo.frontend.modelClientHttp.updateResource( myself ,"sensors", "temperature", "value", guardVars.get("V")  );
	    	}
	    	repeatPlanNoTransition(pr,myselfName,"controller_"+myselfName,false,true);
	    }catch(Exception e_updateTemp){  
	    	 println( getName() + " plan=updateTemp WARNING:" + e_updateTemp.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//updateTemp
	    
	    StateFun updateTime = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("updateTime",-1);
	    	String myselfName = "updateTime";  
	    	printCurrentEvent(false);
	    	//onEvent 
	    	setCurrentMsgFromStore(); 
	    	curT = Term.createTerm("timeChange(V)");
	    	if( currentEvent != null && currentEvent.getEventId().equals("timeChange") && 
	    		pengine.unify(curT, Term.createTerm("timeChange(V)")) && 
	    		pengine.unify(curT, Term.createTerm( currentEvent.getMsg() ) )){ 
	    			String parg="changeModel(hour,V)";
	    			/* PHead */
	    			parg =  updateVars( Term.createTerm("timeChange(V)"), 
	    			                    Term.createTerm("timeChange(V)"), 
	    				    		  	Term.createTerm(currentEvent.getMsg()), parg);
	    				if( parg != null ) {
	    				    aar = QActorUtils.solveGoal(this,myCtx,pengine,parg,"",outEnvView,86400000);
	    					//println(getName() + " plan " + curPlanInExec  +  " interrupted=" + aar.getInterrupted() + " action goon="+aar.getGoon());
	    					if( aar.getInterrupted() ){
	    						curPlanInExec   = "updateTime";
	    						if( aar.getTimeRemained() <= 0 ) addRule("tout(demo,"+getName()+")");
	    						if( ! aar.getGoon() ) return ;
	    					} 			
	    					if( aar.getResult().equals("failure")){
	    						if( ! aar.getGoon() ) return ;
	    					}else if( ! aar.getGoon() ) return ;
	    				}
	    	}
	    	if( (guardVars = QActorUtils.evalTheGuard(this, " !?model(hour,V)" )) != null ){
	    	it.unibo.frontend.modelClientHttp.updateResource( myself ,"sensors", "clock", "value", guardVars.get("V")  );
	    	}
	    	repeatPlanNoTransition(pr,myselfName,"controller_"+myselfName,false,true);
	    }catch(Exception e_updateTime){  
	    	 println( getName() + " plan=updateTime WARNING:" + e_updateTime.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//updateTime
	    
	    StateFun updateConstraints = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("updateConstraints",-1);
	    	String myselfName = "updateConstraints";  
	    	printCurrentMessage(false);
	    	//onMsg 
	    	setCurrentMsgFromStore(); 
	    	curT = Term.createTerm("update(time_bounds(L,U))");
	    	if( currentMessage != null && currentMessage.msgId().equals("update_m") && 
	    		pengine.unify(curT, Term.createTerm("update(X)")) && 
	    		pengine.unify(curT, Term.createTerm( currentMessage.msgContent() ) )){ 
	    		String parg="replaceRule(time_bounds(_,_),time_bounds(L,U))";
	    		/* PHead */
	    		parg =  updateVars( Term.createTerm("update(X)"), 
	    		                    Term.createTerm("update(time_bounds(L,U))"), 
	    			    		  	Term.createTerm(currentMessage.msgContent()), parg);
	    			if( parg != null ) {
	    			    aar = QActorUtils.solveGoal(this,myCtx,pengine,parg,"",outEnvView,86400000);
	    				//println(getName() + " plan " + curPlanInExec  +  " interrupted=" + aar.getInterrupted() + " action goon="+aar.getGoon());
	    				if( aar.getInterrupted() ){
	    					curPlanInExec   = "updateConstraints";
	    					if( aar.getTimeRemained() <= 0 ) addRule("tout(demo,"+getName()+")");
	    					if( ! aar.getGoon() ) return ;
	    				} 			
	    				if( aar.getResult().equals("failure")){
	    					if( ! aar.getGoon() ) return ;
	    				}else if( ! aar.getGoon() ) return ;
	    			}
	    	}
	    	//onMsg 
	    	setCurrentMsgFromStore(); 
	    	curT = Term.createTerm("update(temp_threshold(T))");
	    	if( currentMessage != null && currentMessage.msgId().equals("update_m") && 
	    		pengine.unify(curT, Term.createTerm("update(X)")) && 
	    		pengine.unify(curT, Term.createTerm( currentMessage.msgContent() ) )){ 
	    		String parg="replaceRule(temp_threshold(_),temp_threshold(T))";
	    		/* PHead */
	    		parg =  updateVars( Term.createTerm("update(X)"), 
	    		                    Term.createTerm("update(temp_threshold(T))"), 
	    			    		  	Term.createTerm(currentMessage.msgContent()), parg);
	    			if( parg != null ) {
	    			    aar = QActorUtils.solveGoal(this,myCtx,pengine,parg,"",outEnvView,86400000);
	    				//println(getName() + " plan " + curPlanInExec  +  " interrupted=" + aar.getInterrupted() + " action goon="+aar.getGoon());
	    				if( aar.getInterrupted() ){
	    					curPlanInExec   = "updateConstraints";
	    					if( aar.getTimeRemained() <= 0 ) addRule("tout(demo,"+getName()+")");
	    					if( ! aar.getGoon() ) return ;
	    				} 			
	    				if( aar.getResult().equals("failure")){
	    					if( ! aar.getGoon() ) return ;
	    				}else if( ! aar.getGoon() ) return ;
	    			}
	    	}
	    	repeatPlanNoTransition(pr,myselfName,"controller_"+myselfName,false,true);
	    }catch(Exception e_updateConstraints){  
	    	 println( getName() + " plan=updateConstraints WARNING:" + e_updateConstraints.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//updateConstraints
	    
	    StateFun checkTemp = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("checkTemp",-1);
	    	String myselfName = "checkTemp";  
	    	temporaryStr = "\"[CONTROLLER]	Checks temperature constraint before starting the activity\"";
	    	println( temporaryStr );  
	    	if( (guardVars = QActorUtils.evalTheGuard(this, " !?check_temp" )) != null ){
	    	temporaryStr = "\"[CONTROLLER]	temperature constraint ok\"";
	    	temporaryStr = QActorUtils.substituteVars(guardVars,temporaryStr);
	    	println( temporaryStr );  
	    	}
	    	else{ {//actionseq
	    	temporaryStr = QActorUtils.unifyMsgContent(pengine,"invalid(T)","invalid(temp)", guardVars ).toString();
	    	sendMsg("invalid_m",getNameNoCtrl(), QActorContext.dispatch, temporaryStr ); 
	    	temporaryStr = "\"[CONTROLLER]	temperature constraint NOT ok\"";
	    	println( temporaryStr );  
	    	};//actionseq
	    	}
	    	//bbb
	     msgTransition( pr,myselfName,"controller_"+myselfName,false,
	          new StateFun[]{stateTab.get("startWait") }, 
	          new String[]{"true","M","invalid_m" },
	          500, "checkTime" );//msgTransition
	    }catch(Exception e_checkTemp){  
	    	 println( getName() + " plan=checkTemp WARNING:" + e_checkTemp.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//checkTemp
	    
	    StateFun checkTime = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("checkTime",-1);
	    	String myselfName = "checkTime";  
	    	temporaryStr = "\"[CONTROLLER]	Checks time constraint before starting the activity\"";
	    	println( temporaryStr );  
	    	if( (guardVars = QActorUtils.evalTheGuard(this, " !?check_time" )) != null ){
	    	temporaryStr = "\"[CONTROLLER]	time constraint ok\"";
	    	temporaryStr = QActorUtils.substituteVars(guardVars,temporaryStr);
	    	println( temporaryStr );  
	    	}
	    	else{ {//actionseq
	    	temporaryStr = QActorUtils.unifyMsgContent(pengine,"invalid(T)","invalid(timeout)", guardVars ).toString();
	    	sendMsg("invalid_m",getNameNoCtrl(), QActorContext.dispatch, temporaryStr ); 
	    	temporaryStr = "\"[CONTROLLER]	time constraint NOT ok\"";
	    	println( temporaryStr );  
	    	};//actionseq
	    	}
	    	//bbb
	     msgTransition( pr,myselfName,"controller_"+myselfName,false,
	          new StateFun[]{stateTab.get("startWait") }, 
	          new String[]{"true","M","invalid_m" },
	          500, "waitForSonar1" );//msgTransition
	    }catch(Exception e_checkTime){  
	    	 println( getName() + " plan=checkTime WARNING:" + e_checkTime.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//checkTime
	    
	    StateFun waitForSonar1 = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("waitForSonar1",-1);
	    	String myselfName = "waitForSonar1";  
	    	temporaryStr = "\"[CONTROLLER]	waiting for sonar1\"";
	    	println( temporaryStr );  
	    	//bbb
	     msgTransition( pr,myselfName,"controller_"+myselfName,false,
	          new StateFun[]{stateTab.get("checkDistance") }, 
	          new String[]{"true","E","sonar1" },
	          1000, "startWait" );//msgTransition
	    }catch(Exception e_waitForSonar1){  
	    	 println( getName() + " plan=waitForSonar1 WARNING:" + e_waitForSonar1.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//waitForSonar1
	    
	    StateFun checkDistance = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("checkDistance",-1);
	    	String myselfName = "checkDistance";  
	    	//onEvent 
	    	setCurrentMsgFromStore(); 
	    	curT = Term.createTerm("sonar(X)");
	    	if( currentEvent != null && currentEvent.getEventId().equals("sonar1") && 
	    		pengine.unify(curT, Term.createTerm("sonar(D)")) && 
	    		pengine.unify(curT, Term.createTerm( currentEvent.getMsg() ) )){ 
	    			String parg="replaceRule(distance(_),distance(X))";
	    			/* PHead */
	    			parg =  updateVars( Term.createTerm("sonar(D)"), 
	    			                    Term.createTerm("sonar(X)"), 
	    				    		  	Term.createTerm(currentEvent.getMsg()), parg);
	    				if( parg != null ) {
	    				    aar = QActorUtils.solveGoal(this,myCtx,pengine,parg,"",outEnvView,86400000);
	    					//println(getName() + " plan " + curPlanInExec  +  " interrupted=" + aar.getInterrupted() + " action goon="+aar.getGoon());
	    					if( aar.getInterrupted() ){
	    						curPlanInExec   = "checkDistance";
	    						if( aar.getTimeRemained() <= 0 ) addRule("tout(demo,"+getName()+")");
	    						if( ! aar.getGoon() ) return ;
	    					} 			
	    					if( aar.getResult().equals("failure")){
	    						if( ! aar.getGoon() ) return ;
	    					}else if( ! aar.getGoon() ) return ;
	    				}
	    	}
	    	if( (guardVars = QActorUtils.evalTheGuard(this, " !?checkDistance" )) != null ){
	    	temporaryStr = QActorUtils.unifyMsgContent(pengine,"distanceOk","distanceOk", guardVars ).toString();
	    	sendMsg("distanceOk",getNameNoCtrl(), QActorContext.dispatch, temporaryStr ); 
	    	}
	    	//bbb
	     msgTransition( pr,myselfName,"controller_"+myselfName,false,
	          new StateFun[]{stateTab.get("sendStart") }, 
	          new String[]{"true","M","distanceOk" },
	          200, "startWait" );//msgTransition
	    }catch(Exception e_checkDistance){  
	    	 println( getName() + " plan=checkDistance WARNING:" + e_checkDistance.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//checkDistance
	    
	    StateFun sendStart = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("sendStart",-1);
	    	String myselfName = "sendStart";  
	    	temporaryStr = "\"[CONTROLLER]	RUNNING ROBOT\"";
	    	println( temporaryStr );  
	    	temporaryStr = QActorUtils.unifyMsgContent(pengine,"startCleaning","startCleaning", guardVars ).toString();
	    	sendMsg("startCleaning","mind", QActorContext.dispatch, temporaryStr ); 
	    	temporaryStr = QActorUtils.unifyMsgContent(pengine, "blink(S)","blink(true)", guardVars ).toString();
	    	emit( "blink", temporaryStr );
	    	//switchTo running
	        switchToPlanAsNextState(pr, myselfName, "controller_"+myselfName, 
	              "running",false, false, null); 
	    }catch(Exception e_sendStart){  
	    	 println( getName() + " plan=sendStart WARNING:" + e_sendStart.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//sendStart
	    
	    StateFun running = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp(getName()+"_running",0);
	     pr.incNumIter(); 	
	    	String myselfName = "running";  
	    	//bbb
	     msgTransition( pr,myselfName,"controller_"+myselfName,false,
	          new StateFun[]{stateTab.get("updateTemp"), stateTab.get("updateTime"), stateTab.get("updateConstraints"), stateTab.get("stopEverything"), stateTab.get("stopEverything") }, 
	          new String[]{"true","E","tempChange", "true","E","timeChange", "true","M","update_m", "true","M","invalid_m", "true","M","cleaningCompleted" },
	          500, "running" );//msgTransition
	    }catch(Exception e_running){  
	    	 println( getName() + " plan=running WARNING:" + e_running.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//running
	    
	    StateFun stopEverything = () -> {	
	    try{	
	     PlanRepeat pr = PlanRepeat.setUp("stopEverything",-1);
	    	String myselfName = "stopEverything";  
	    	temporaryStr = "\"[CONTROLLER]	STOPPING ROBOT\"";
	    	println( temporaryStr );  
	    	temporaryStr = QActorUtils.unifyMsgContent(pengine,"stopCleaning","stopCleaning", guardVars ).toString();
	    	sendMsg("stopCleaning","mind", QActorContext.dispatch, temporaryStr ); 
	    	temporaryStr = QActorUtils.unifyMsgContent(pengine, "blink(S)","blink(false)", guardVars ).toString();
	    	emit( "blink", temporaryStr );
	    	//switchTo startWait
	        switchToPlanAsNextState(pr, myselfName, "controller_"+myselfName, 
	              "startWait",false, false, null); 
	    }catch(Exception e_stopEverything){  
	    	 println( getName() + " plan=stopEverything WARNING:" + e_stopEverything.getMessage() );
	    	 QActorContext.terminateQActorSystem(this); 
	    }
	    };//stopEverything
	    
	    protected void initSensorSystem(){
	    	//doing nothing in a QActor
	    }
	
	}
