function log(message) {
	document.getElementById("cmdconsole").innerHTML += "QActorBridge.js:\t" + message + '<br/>';
}
 
var curSpeed = "low";
function setSpeed(val){
		curSpeed = val;
}

/* SET UP WEBSOCKET */
var connected = false;
var host = "localhost:8080";
var sock;
addLoadEvent(function() {
	log("server IP= "+ host + " connected=" + connected);    
	sock = new WebSocket("ws://" + host, "protocolOne");
	sock.onopen = function (event) {
		log("Connection established");
		connected = true;
	};
	sock.onmessage = function (event) {
		log('sent message: ' +event.data);
	}
	sock.onerror = function (event) {
		log('WebSocket Error: ' +  event);
	};
});

function send(message) {
		if( connected ){
			log("Sending message: " + message);
			sock.send(message);
		}
		else {
			log("Can't send message: " +  message);
		}
};