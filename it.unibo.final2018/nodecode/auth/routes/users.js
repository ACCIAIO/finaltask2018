var express = require('express');
var router = express.Router();
var User = require('../models/user');

/* GET users listing. */
router.get('/', function(req, res, next) {	
	User.find({}).exec(function(err, users){
		res.render('users', {title: 'Users', user: req.session.user, users: users});
	});
});

router.get('/profile', function(req, res, next){
	var user;
	if(req.session.user){
		user = req.session.user;
	}
	
	if(req.query.username){		
		User.findOne({username: req.query.username}).exec(function(err, target){
			if(err){
				next(err);
			}
			if (target == null) {
				var err = new Error('No user with username ' + req.query.username);
				err.status = 401;
				next(err);
			}
			res.render('profile', {title: "Profile of " + req.query.username, user: user, target: target})
		});
	}else{
		var err = new Error('Missing username.');
		err.status = 401;
		next(err);
	}
});

router.get('/profile/edit', function(req, res, next){
	if(req.session.user){
		// Se non sei loggato, non puoi modificare alcun profilo
		if (req.query.username) {
			User.findOne({username: req.query.username}).exec(function(err, targetUser){
				if (err) {
					next(err);
				}
				if(targetUser == null){
					var err = new Error("Invalid username.");
					err.status = 401;
					next(err);
				}
				// Se hai i diritti puoi modificare
				if(req.session.user.admin || req.query.username == req.session.user.username){
					res.render('profile_edit', {title: "Editing profile of " + req.query.username, user: req.session.user, target: targetUser});
				}else {
					var err = new Error("You don't have the permissions needed to edit this profile.");
					err.status = 401;
					next(err);
				}
			});
		} else {
			var err = new Error("Missing username.");
			err.status = 401;
			next(err);
		}
	} else {
		res.redirect('/login');
	}
});

/* Submit Profile Edits */
router.post('/profile/edit/submit', function(req, res, next){
	if(req.session.user){
		if(req.session.user.admin || req.session.user.username == req.body.username){
			var query = {'username': req.body.username};
			
			var newData = {};
			
			if(req.body.name){
				newData.name = req.body.name;
			}
			
			if(req.body.last_name){
				newData.last_name = req.body.last_name;
			}
			
			if(req.body.email){
				newData.email = req.body.email;
			}
			
			if(req.body.role){
				newData.role = req.body.role;
			}
			
			if(req.body.bio){
				newData.bio = req.body.bio;
			}
			
			if(req.body.admin){
				newData.admin = true;
			}else{
				newData.admin = false;
			}
			
			User.findOneAndUpdate(query, newData, {upsert: false}, function(err, doc){
				if(err){
					next(err);
				}
				return res.redirect('/users/profile?username='+req.body.username);
			});
		} else {
			var err = new Error("You don't have the permissions to edit this profile.");
			err.status = 401;
			next(err);
		}	
	} else {
		res.redirect('/login');
	}	
});

/* Profile deletion request */
router.get('/profile/delete', function(req, res, next){
	if (req.session.user) {
		if(req.session.user.admin || req.session.user.username == req.query.username) {
			User.deleteOne({'username': req.query.username}, function(err,doc) {
				if(err){
					next(err);
				}
				if(req.session.user.admin){
					return res.redirect('/users');
				}else{
					return res.redirect('/logout');
				}
			});
		} else {
			var err = new Error("You don't have the permissions to delete this profile.");
			err.status = 401;
			next(err);
		}	
	} else {
		res.redirect('/login');
	}
});

module.exports = router;
