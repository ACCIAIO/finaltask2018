var express = require('express');
var router = express.Router();
var User = require('../models/user');
resources = require('../models/resources');

/* POST login*/
router.post('/request', function(req, res, next){
	//console.log('Richiesta di login: '+req.body.username+", pwd: "+req.body.pwd);
	if (req.session.user) {
		var err = new Error("You are already logged in.");
		next(err);
	} else if(req.body.username && req.body.pwd){
		User.authenticate(req.body.username, req.body.pwd, function(err, user){
			if(err){
				next(err);
			}else{
				req.session.user = user;
				if (req.body.destination) {
					res.redirect('/' + req.body.destination);
				} else {
					res.redirect('/welcome');
				}
			}
		});
	}
});


/* Login */
router.get('/', function(req, res, next){
	if(req.session.user){
		//Already logged in
		var err = new Error("Already logged in.");
        err.status = 401;
        return next(err);

	}else{		
		res.render('login', { title: 'Login' });
	}
});

module.exports = router;
