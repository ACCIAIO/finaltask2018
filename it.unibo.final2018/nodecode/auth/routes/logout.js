var express = require('express');
var router = express.Router();
var User = require('../models/user');

/* Logout*/
router.get('/', function(req, res){
	if(req.session.user){
		console.log(req.session.user.username + 'logged out.');
		delete req.session.user;
	}
	res.render('logout_success', {title: 'Uscito con successo', user: undefined});
});

module.exports = router;
