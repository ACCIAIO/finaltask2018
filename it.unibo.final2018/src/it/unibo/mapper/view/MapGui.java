package it.unibo.mapper.view;

import javax.swing.JScrollPane;

import it.aperture.swing.Frame;
import it.aperture.swing.Panel;

public class MapGui extends Frame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Panel panel = new Panel();
	private JScrollPane scroller;
	
	public Panel getCanvas() {
		return panel;
	}
	
	public MapGui() {
		this.setTitle("MAP");
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(Frame.EXIT_ON_CLOSE);
		panel.setSize(1000, 1000);
		panel.setPreferredSize(panel.getSize());
		scroller = new JScrollPane(panel);
		scroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		this.setContentPane(scroller);
		this.setSize(600, 400);
	}
	
}
