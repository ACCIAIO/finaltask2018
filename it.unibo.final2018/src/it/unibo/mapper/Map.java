package it.unibo.mapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.commons.lang3.tuple.Pair;

public class Map {
	
	public static final byte UNKNOWN = 0, NAVIGATED = 1, UNREACHABLE = 2; 
	private final static java.util.Map<String, Pair<Integer, Integer>> directionsToOffset;
	
	static {
		directionsToOffset = new HashMap<>();
		directionsToOffset.put("up", Pair.of(0, -1));
		directionsToOffset.put("right", Pair.of(1, 0));
		directionsToOffset.put("down", Pair.of(0, 1));
		directionsToOffset.put("left", Pair.of(-1, 0));
	}
	
	public static Map fromFile(File f) throws IOException {
		InputStream in = new FileInputStream(f);
		byte[] buffer = new byte[4];
		in.read(buffer);
		int len = ByteBuffer.wrap(buffer).getInt();
		Map map = new Map();
		for(int i = 0; i < len; i++) {
			buffer = new byte[len];
			in.read(buffer);
			for(int k = 0; k < len; k++)
				map.discoverSlot(i, k, buffer[k]);
		}
		in.close();
		return map;
	}
	
	private  byte[][] map;
	private final int increment;
	
	private int bX, bY;
	
	private void ensureCapacity(int x, int y) {
		byte[][] tmp = map;
		if(map.length <= x || map[0].length <= y) {
			int nl = map.length + increment; 
			map = new byte[nl][nl];
			for(int i = 0; i < tmp.length; i++) {
				map[i] = Arrays.copyOf(tmp[i], nl);
			}
		}
	}
	
	public int getSize() {
		return map.length;
	}
	
	public int getBoundaryX() {
		return bX;
	}
	
	public int getBoundaryY() {
		return bY;
	}
	
	public byte getSlot(int x, int y) {
		if(0 > x || 0 > y)
			throw new IllegalArgumentException("coordinates must be positive");
		if(map.length <= x || map[0].length <= y)
			return UNKNOWN;
		return map[x][y];
	}
	
	public int getSlotRelativeTo(int x, int y, String direction) {
		Pair<Integer, Integer> p = directionsToOffset.get(direction);
		return getSlot(x + p.getLeft(), y + p.getRight());
	}
	
	public void discoverSlot(int x, int y, int value) {
		if(0 > x || 0 > y)
			throw new IllegalArgumentException("coordinates must be positive");
		if(UNKNOWN > value || UNREACHABLE < value)
			throw new IllegalArgumentException("only acceptable values are UNKNOWN, NAVIGATED or UNREACHABLE");
		
		ensureCapacity(x, y);
		
		if(map[x][y] != UNKNOWN)
			return;
		
		map[x][y] = (byte)value;
		
		if(UNREACHABLE == value) {
			bX = (bX > x) ? bX : x;
			bY = (bY > y) ? bY : y;
		}
	}
	
	public Map() {
		this(32);
	}
	
	public Map(int initialDimension) {
		if(0 >= initialDimension)
			throw new IllegalArgumentException("the initial dimension must be strictly positive");
		increment = initialDimension;
		reset();
	}
	
	public void reset() {
		map = new byte[increment][increment];
		bX = bY = -1;
	}
	
	public void save(File f) {
		try {
			OutputStream out = new FileOutputStream(f);
			
			out.write(ByteBuffer.allocate(4).putInt(map.length).array());
			for(int i = 0; i < map.length; i++) {
				out.write(map[i]);
			}
			
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

}
