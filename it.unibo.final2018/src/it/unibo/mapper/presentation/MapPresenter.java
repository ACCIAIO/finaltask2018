package it.unibo.mapper.presentation;


import java.awt.Color;
import java.awt.Graphics;
import java.util.HashMap;

import it.aperture.swing.PaintEventArgs;
import it.unibo.mapper.Map;
import it.unibo.mapper.view.MapGui;

public class MapPresenter {

	private final MapGui gui;
	private final Map map;
	
	private final HashMap<Byte, Color> colors = new HashMap<>();
	
	public MapPresenter(MapGui gui, Map map) {
		this.gui = gui;
		this.map = map;
		
		colors.put(Map.UNKNOWN, Color.gray);
		colors.put(Map.NAVIGATED, Color.white);
		colors.put(Map.UNREACHABLE, Color.red);
		
		init();
	}
	
	private void init() {
		gui.getCanvas().getPainted().add(this::onPaint);
	}
	
	public void update() {
		gui.getCanvas().setSize(32 * map.getSize(), 32 * map.getSize());
		gui.getCanvas().setPreferredSize(gui.getCanvas().getSize());
		gui.getCanvas().repaint();
	}
	
	private void onPaint(PaintEventArgs args) {
		Graphics g = args.getGraphics();
		for(int i = 0; i < map.getSize(); i++)
			for(int k = 0; k < map.getSize(); k++) {
				byte value = map.getSlot(i, k);
				g.setColor(colors.get(value));
				g.fillRect(32 * i, 32 * k, 32, 32);
				g.setColor(Color.black);
				g.drawRect(32 * i, 32 * k, 32, 32);
			}
	}
}
