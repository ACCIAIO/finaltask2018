package it.unibo.mapper;

import java.awt.Point;
import java.io.File;

import javax.swing.SwingUtilities;

import it.unibo.mapper.presentation.MapPresenter;
import it.unibo.mapper.view.MapGui;
import it.unibo.qactors.akka.QActor;

public class mapping {

	private static MapPresenter presenter;
	private static Map map;

	private static boolean stairX = true;
	private static boolean avoidingObstacle = false;

	public static void init(QActor actor) {
		map = new Map();
	}

	public static void init(QActor actor, String initialDimension) {
		map = new Map(Integer.parseInt(initialDimension));
	}

	public static void initGui(QActor actor) {
		MapGui gui = new MapGui();
		presenter = new MapPresenter(gui, map);
		SwingUtilities.invokeLater(() -> gui.setVisible(true));
	}

	public static void update(QActor actor, String x, String y, String value) {
		map.discoverSlot(Integer.parseInt(x), Integer.parseInt(y), Integer.parseInt(value));
		if (null != presenter)
			presenter.update();
	}

	public static void next(QActor actor, String sx, String sy, String d) {
		int x = Integer.parseInt(sx);
		int y = Integer.parseInt(sy);

		Point p = getNearestUnknown(x, y);
		Point limit = new Point(map.getBoundaryX() - 1, map.getBoundaryY() - 1);

		String m;

		if (avoidingObstacle) {
			m = "w";
			avoidingObstacle = false;
		} else if (limit.equals(new Point(x,y)))
			m = "h";
		else if (x == p.x)
			m = moveAlongY(y, p, d);
		else if (y == p.y)
			m = moveAlongX(x, p, d);
		else {
			if (stairX)
				m = moveAlongY(y, p, d);
			else
				m = moveAlongX(x, p, d);

			if (m.equals("w"))
				stairX = !stairX;
		}

		if ("w".equals(m) && Map.UNREACHABLE == map.getSlotRelativeTo(x, y, d))
			m = handleObstacle(x, y, p, d);

		try {
			actor.automsg("next", "dispatch", "next(" + m + ")");
		} catch (Exception e) {
			actor.emit("error", "automessage");
			e.printStackTrace();
		}
	}

	public static void adjustMovement(QActor actor, String sx, String sy){
		int x = Integer.parseInt(sx);
		int y = Integer.parseInt(sy);
		
		if(map.getSlot(x, y) == Map.NAVIGATED){
			try {
				actor.automsg("adjust", "dispatch", "adjusted");
			} catch (Exception e) {
				actor.emit("error", "automessage");
				e.printStackTrace();
			}
		}
	}
	
	public static void mappingEnd(QActor actor) {
		if(null == map)
			return;
		map.save(new File("map.rmap"));
		map.reset();
		if(null != presenter)
			presenter.update();
	}
	
	// UTILITY METHODS FROM HERE ON

	private static String handleObstacle(int x, int y, Point p, String d) {
		String m;
		if ("right".equals(d))
			if (p.y < y)
				m = "a";
			else
				m = "d";
		else if ("left".equals(d))
			if (p.y < y)
				m = "d";
			else
				m = "a";
		else if ("down".equals(d))
			if (p.x < x)
				m = "d";
			else
				m = "a";
		else if (p.x < x)
			m = "a";
		else
			m = "d";

		avoidingObstacle = true;
		return m;
	}

	private static String moveAlongY(int y, Point p, String d) {
		return y < p.y ? RobotReferencingResolver.resolve("down", d) : RobotReferencingResolver.resolve("up", d);
	}

	private static String moveAlongX(int x, Point p, String d) {
		return x < p.x ? RobotReferencingResolver.resolve("right", d) : RobotReferencingResolver.resolve("left", d);
	}

	private static Point getNearestUnknown(int x, int y) {

		for (int i = 1; i < map.getSize() / 2; i++) {
			int sX = (0 >= x - i) ? 1 : x - i;
			int sY = (0 >= y - i) ? 1 : y - i;
			for (int j = sX; j < x - i + 2 * i + 1 && j < map.getBoundaryX(); j++) {
				for (int k = sY; k < y - i + 2 * i + 1 && k < map.getBoundaryY(); k++) {
					if (0 == map.getSlot(j, k))
						return new Point(j, k);
				}
			}
		}

		return new Point(map.getBoundaryX() - 1, map.getBoundaryY() - 1);
	}

}
