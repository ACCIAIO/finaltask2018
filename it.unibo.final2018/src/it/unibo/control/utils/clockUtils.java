package it.unibo.control.utils;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Timer;
import java.util.TimerTask;

import it.unibo.qactors.akka.QActor;

public class clockUtils {
	private static Timer timer;

	public static void register(QActor actor) {
		long missing = Duration.between(LocalDateTime.now(), LocalDateTime.now().plusHours(1).truncatedTo(ChronoUnit.HOURS)).toMillis();
		System.out.println(missing);
		if (null != timer)
			timer.cancel();
		timer = new Timer();
		TimerTask tt = new TimerTask() {

			@Override
			public void run() {
				System.out.println("[CLOCK]\thour strike");
				int h = LocalTime.now().getHour();
				actor.emit("timeChange", "timeChange(" + h + ")");
			}

		};
		timer.scheduleAtFixedRate(tt, missing, 3600000);
	}

	public static void forceEmit(QActor actor) {
		int h = LocalTime.now().getHour();
		actor.emit("timeChange", "timeChange(" + h + ")");
	}
}
