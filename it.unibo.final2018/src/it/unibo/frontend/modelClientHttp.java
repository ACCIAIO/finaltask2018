package it.unibo.frontend;

import it.unibo.qactors.akka.QActor;

public class modelClientHttp {

	/***
	 * 
	 * @param qa	lascia stare
	 * @param host	host
	 * @param type	sensors or actuator?
	 * @param id	nome del sensore o attuatore
	 * @param field	nome del campo da modificare
	 * @param value	valore da scrivere nel campo in questione
	 */
	public static void updateResource(QActor qa, String host, String type, String id, String field, String value) {
		restClientHttp.connectGet(qa, "http://" + host + ":3000/settings/resources?"+
				"type="+ type +
				"&id=" + id +
				"&field=" + field +
				"&value=" + value);
	}
	
	public static void updateResource(QActor qa, String type, String id, String field, String value) {
		updateResource(qa,"localhost",type,id,field,value);
	}
	
	/***
	 * 
	 * @param qa	lascia stare
	 * @param host	host
	 * @param id	0 - sonarRobot, 1 - sonar1, 2 - sonar2
	 * @param value	valore da associare al sonar
	 */
	public static void updateRobot(QActor qa, String host, String id, String value) {
		restClientHttp.connectGet(qa, "http://" + host + ":3000/settings/robot/sonar?" +
				"id=" + id +
				"&value=" + value);
				
	}
	
	public static void updateRobot(QActor qa, String id, String value) {
		restClientHttp.connectGet(qa, "http://localhost:3000/settings/robot/sonar?" +
				"id=" + id +
				"&value=" + value);
				
	}
}
