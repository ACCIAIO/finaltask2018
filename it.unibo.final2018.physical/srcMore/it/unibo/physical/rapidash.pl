%====================================================================================
% Context physical  SYSTEM-configuration: file it.unibo.physical.rapidash.pl 
%====================================================================================
context(physical, "localhost",  "TCP", "9004" ).  		 
%%% -------------------------------------------
qactor( rover , physical, "it.unibo.rover.MsgHandle_Rover"   ). %%store msgs 
qactor( rover_ctrl , physical, "it.unibo.rover.Rover"   ). %%control-driven 
qactor( led_manager , physical, "it.unibo.led_manager.MsgHandle_Led_manager"   ). %%store msgs 
qactor( led_manager_ctrl , physical, "it.unibo.led_manager.Led_manager"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

