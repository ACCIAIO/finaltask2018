package it.unibo.controls;

import it.unibo.qactors.akka.QActor;

public class physicalControls {
	
	public static void moveRobot(QActor qa, String move) {
		try { 		
			switch( move ) {
				case "w" :{
					it.unibo.rover.mbotConnArduino.mbotForward(qa);
					break;
				}
				case "s" :{
 					it.unibo.rover.mbotConnArduino.mbotBackward(qa);
					break;
				}
				case "a" :{ 
					it.unibo.rover.mbotConnArduino.mbotLeft(qa);
					break;
				}
				case "d" :{
					it.unibo.rover.mbotConnArduino.mbotRight(qa);
					break;
				}
				case "h" :{
					it.unibo.rover.mbotConnArduino.mbotStop(qa);
					break;
				}	 				  
			}						
 		} catch (Exception e) { e.printStackTrace(); } 
	}
	
}
